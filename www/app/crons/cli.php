<?php
use \Phalcon\CLI\Console as ConsoleApp;
use \Phalcon\Di\FactoryDefault\Cli as CliDI;

defined('IS_CLI') || define("IS_CLI", true);

// Include the constants
include __DIR__ . "/../config/constants.php";

// Set up Composer autoloader
require_once ROOT_PATH . '/vendor/autoload.php';

/**
 * Read the configuration
 */
$config = include APP_PATH . "/config/appconfig/config.php";

/**
 * Setup redis
 */
\Resque::setBackend($config->application->redis->hostName . $config->application->domainName . ':' . $config->application->redis->port);
\Resque::redis()->auth($config->application->redis->auth);

/**
 * Read auto-loader
 */
include APP_PATH . "/config/loader.php";

/**
 * Services
 */
include APP_PATH . "/config/services.php";

$console = new ConsoleApp();
// Create a console application
$di = new CliDI();
$console->setDI($di);

/**
 * Process the console arguments
 */
$arguments = array();
foreach ($argv as $k => $arg) {
    if ($k == 1) {
        $arguments['task'] = $arg;
    } elseif ($k == 2) {
        $arguments['action'] = $arg;
    } elseif ($k >= 3) {
        $arguments[] = $arg;
    }
}

// define global constants for the current task and action
define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

// $di->setShared('console', $console);

try {
    // handle incoming arguments
    $console->handle($arguments);
} catch (\Phalcon\Exception $e) {
    echo $e->getMessage()."\n";
    exit(255);
}