<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/18/2015
 * Time: 2:33 PM
 */
class GitTask extends \Phalcon\CLI\Task
{
    /**
     * main function to be called
     */
    public function mainAction()
    {
        $list = "Application got hooks run: \n";
        $methods = get_class_methods($this);
        foreach ($methods as $method) {
            if (! preg_match("/(__|set|get|main)/i", $method)) {
                $method = str_replace('Action', '', $method);
                $list .= "Call : {$method} - COMMAND: php cli.php app {$method} \n";
            }
        }
        echo $list . "\n";
    }

    /**
     * After making a pull request
     */
    public function afterPullAction()
    {
        // You can add functionality here, what it is to be done after pull
        echo 'After pull completed' . PHP_EOL;
        echo '>>>>CHECK IF YOU HAVE TO RESTART THE WORKERS!!!' . PHP_EOL;
    }

    /**
     * After applying migration(s)
     */
    public function onDbMigrationAppliedAction()
    {
        $services = Phalcon\Di::getDefault();

        try
        {
            echo PHP_EOL . 'Reset routes cache ... ' . PHP_EOL;
            $services->get('Proxy_Internal_Routes')->resetCache();
            echo 'Routes cache has been reset with success' . PHP_EOL;

            echo PHP_EOL . 'Reset acl cache ... ' . PHP_EOL;
            $services->get('Proxy_Internal_Acl')->resetAclCache();
            echo 'Acl cache has been reset with success' . PHP_EOL;
        }
        catch(\Exception $e) {
            echo 'Something happened while trying to reset the cache: ' . $e->getMessage() . PHP_EOL;
        }
    }
}
