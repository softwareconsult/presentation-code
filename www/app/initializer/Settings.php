<?php
/**
 * Handles application settings environment based
 *
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/19/14
 * Time: 4:25 PM
 */

namespace App\Initializer;

class Settings
{
    /**
     * The settings
     * @var array
     */
    protected $data = null;

    /**
     * @var string
     */
    protected $environment = 'development';

    /**
     * @var string
     */
    protected $settingsFile = null;

    /**
     * @param null|string|array $settings
     * @param null|string       $env
     */
    public function __construct($settings = null, $env = null)
    {
        $this->setEnvironment($env);

        $this->setData($settings);
    }

    /**
     * @param array $data
     */
    protected function setData($data)
    {
        if (!is_array($data)) {
            $this->setSettingsFile($data);
            $data = include $this->getSettingsFile();
        }

        $mergedSettings = array();

        // Merge the environments settings with the latest more important
        foreach ($data as $env => $settings) {
            if (!empty($settings)) {
                $mergedSettings = array_replace_recursive($mergedSettings, $settings);
            }

            if ($env === $this->getEnvironment()) {
                break;
            }
        }

        $this->data = $mergedSettings;
    }

    /**
     * You can declare settings keys split by '.' if the settings needed is deeper
     *
     * @param null|string $key
     *
     * @return array|null
     */
    public function getSettings($key = null) {

        $tmpSettings = $this->data;

        if (is_string($key)) {
            if (strpos($key, '.') !== false) {
                $keys = explode('.', $key);
                foreach ($keys as $part) {
                    if (isset($tmpSettings[$part])) {
                        $tmpSettings = $tmpSettings[$part];
                    } else {
                        return null;
                    }
                }
            }
        }

        return $tmpSettings;
    }

    /**
     * @param string $environment
     */
    protected function setEnvironment($environment = null)
    {
        if (!is_null($environment)) {
            $this->environment = $environment;
        } elseif (defined('APP_ENV')) {
            $this->environment = APP_ENV;
        }
    }

    /**
     * @return string
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param string $settingsFile
     */
    protected function setSettingsFile($settingsFile)
    {        
        $this->settingsFile = $settingsFile;
    }

    /**
     * @return string
     */
    public function getSettingsFile()
    {
        return $this->settingsFile;
    }

}