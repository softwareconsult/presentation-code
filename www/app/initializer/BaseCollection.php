<?php
namespace App\Initializer;

/**
 * Used for MongoDb collections
 */
abstract class BaseCollection
{
    /**
     *
     * @var \Phalcon\Db\Adapter\Pdo\Mysql
     */
    protected $mongo;
    
    /**
     *
     * @var unknown
     */
    protected $services;

    public function __construct()
    {
        $this->services = \Phalcon\Di::getDefault();
        $this->mongo = $this->services->get('mongo');
    }

    /**
     *
     * @param \Phalcon\Mvc\Model $items
     *
     * @return array|null
     */
    protected function toArray($items)
    {
        $array = array();
        if ($items) {
            foreach ($items as $key => $val) {
                $array[$key] = $val;
            } 
            return $array;
        }
        return null;
    }
}