<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 1/12/2016
 * Time: 12:59 PM
 */

namespace App\Initializer;

class BaseTable  extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
        $this->setReadConnectionService('dbRead');
        $this->setWriteConnectionService('dbWrite');
    }

    public function getErrors()
    {
        return $this->_errorMessages;
    }
}