<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/30/14
 * Time: 9:49 AM
 */
namespace App\Initializer;

use App\Helper\CacheHelper;

/**
 * @property mixed STATUS_DELETED
 */
abstract class BaseModel
{
    /**
     * @var \Phalcon\Db\Adapter\Pdo\Mysql
     */
    protected $dbWrite;

    /**
     * @var \Phalcon\Db\Adapter\Pdo\Mysql
     */
    protected $dbRead;

    /**
     * @var \Phalcon\Mvc\Model\Manager
     */
    protected $modelsManager;
    /**
     * @var unknown
     */
    protected $services;

    protected $maxNrOfTries = 3;

    /**
     * @var string The namespace of the current model
     */
    protected $cacheNamespace;

    /**
     * @var \Library\Cache\Backend\Redis
     */
    protected $cache;

    public function __construct()
    {
        $this->services      = \Phalcon\Di::getDefault();
        $this->dbWrite       = $this->services->get('dbWrite');
        $this->dbRead        = $this->services->get('dbRead');
        $this->modelsManager = $this->services->get('modelsManager');
        $this->cache         = $this->services->get('cache');
    }

    /**
     * @param $items
     *
     * @return array|null
     */
    protected function toArray($items)
    {
        if ($items) {
            return $items->toArray();
        }

        return null;
    }

    /**
     * @param \Phalcon\Mvc\Model\Query\BuilderInterface $query
     * @param bool                                      $alternative
     *
     * @return string
     */
    public function debugQuery($query, $alternative = false)
    {
        if (!$alternative) {
            return $this->dbWrite->getDialect()->select($query->getQuery()->parse());
        }

        $intermediate = $query->getQuery()->parse();
        $dialect      = $this->dbWrite->getDialect();

        return $dialect->select($intermediate);
    }

    /**
     * @param $columnName
     * @param $values
     *
     * @return string
     */
    public function escapeWhereInCondition($columnName, $values)
    {
        if (!empty($values)) {
            foreach ($values as $key => $value) {
                $values[$key] = $this->dbWrite->escapeString($value);
            }
        } else {
            $values = array(0);
        }

        $valuesString = implode(',', $values);

        return $columnName . ' IN (' . $valuesString . ')';
    }

    /**
     * Create the cache id from parameters
     *
     * @param string $namespace
     * @param array  $filters
     * @param string $prefix
     *
     * @return string
     */
    protected function getCacheId($namespace, $filters = array(), $prefix = '')
    {
        return CacheHelper::getCacheId($namespace, $filters, $prefix);
    }

    /**
     * Save the data in cache
     *
     * @param $namespace
     * @param $cacheId
     * @param $data
     *
     * @throws \Phalcon\Cache\Exception
     */
    protected function saveCache($namespace, $cacheId, $data)
    {
        CacheHelper::saveCache($namespace, $cacheId, $data);
    }

    /**
     * Delete cache by namespace
     *
     * @param $namespace
     */
    public function deleteCacheByNamespace($namespace = null)
    {
        CacheHelper::deleteCacheByNamespace($namespace);
    }
}