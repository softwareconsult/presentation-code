<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/30/14
 * Time: 3:48 PM
 */
namespace App\Initializer;

abstract class BaseListener
{
    /**
     * @var \Phalcon\DI\FactoryDefault
     */
    protected $services = null;

    public function __construct()
    {
        $this->services = \Phalcon\Di::getDefault();
    }
}