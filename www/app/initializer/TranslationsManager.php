<?php
/**
 * Handles translations so we will include only the ones needed for the current request, not all
 *
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/26/14
 * Time: 4:26 PM
 */

namespace App\Initializer;

class TranslationsManager
{
    /**
     * @var \Phalcon\Translate\AdapterInterface
     */
    protected $translator = null;

    /**
     * @var \App\Initializer\Translations\Translate;
     */
    protected $adapter;

    public function __construct($options)
    {

        if(!isset($options['adapterType'])) {
            $options['adapterType'] = 'NativeArray';
        }

        $adapter = '\\App\\Initializer\\Translations\\' . $options['adapterType'];
        $this->adapter = new $adapter($options);

        $this->setTranslator($this->adapter->getTranslator());
    }
    /**
     * @param \Phalcon\Translate\AdapterInterface $translator
     */
    public function setTranslator(\Phalcon\Translate\AdapterInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return \Phalcon\Translate\AdapterInterface
     */
    public function getTranslator()
    {
        return $this->translator;
    }

}