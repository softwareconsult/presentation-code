<?php

namespace App\Initializer;

use Library\FileSystem\File;
use Library\FileSystem\Folder;
use phpDocumentor\Parser\Exception;
use Phalcon\Logger\Adapter\File as LogsFile;

abstract class BaseJob
{
    /**
     * 
     * @var \Phalcon\DI\FactoryDefault
     */
    protected $services;

    /**
     * @var \Phalcon\Logger\Adapter\File
     */
    public $logger;
    
    public function __construct()
    {
        $this->services = \Phalcon\Di::getDefault();
        $logFile = $this->services->get('file')->create($this->_getLoggerFile());

        $config = $this->services->get('app_config');
        /**
         * Setup redis
         */
        \Resque::setBackend($config->application->redis->hostName . $config->application->domainName . ':' . $config->application->redis->port);
        \Resque::redis()->auth($config->application->redis->auth);
        $this->logger = new LogsFile($logFile);
        $this->init();
    }
    
    public function init() {}

    public function perform()
    {
        try {
            $this->process($this->args);
            $this->logger->info("The async job  " . get_class($this) . " has executed with success with arguments: " . print_r($this->args, true));
        } catch(\Exception $e) {
            $this->logger->error("The async job " . get_class($this) . "has error with message: " . $e->getMessage() . "  TRACE_ROUTE: " . print_r($e->getTraceAsString(), true));
        }
    }

    /**
     * Makes the actual work
     * @param null $args
     */
    public function process($args = null) {}

    /**
     * Get the logger file
     *
     * @return string
     */
    private function _getLoggerFile()
    {
        $dir = date('Y-m');

        Folder::getInstance()->makeDir($this->services->get('app_config')->application->dirs->jobsLogDir, $dir, 0777);
        $logDir = $this->services->get('app_config')->application->dirs->jobsLogDir . "/" . $dir;

        return $logDir . '/' . date('Y-m-d') . '.log';
    }
}