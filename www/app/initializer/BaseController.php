<?php

/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/19/14
 * Time: 2:05 PM
 */

namespace App\Initializer;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\View;

class BaseController extends Controller
{
    /**
     * @var \Phalcon\DI\FactoryDefault
     */
    protected $services;

    /**
     * @var \App\Api\Stylish
     */
    protected $stylishApi;

    /**
     * @var \Phalcon\Acl\Adapter\Memory
     */
    protected $acl;

    /**
     * Sets the event manager
     *
     * @param mixed $eventsManager
     */
    public function setEventsManager(\Phalcon\Events\ManagerInterface $eventsManager)
    {
        $this->_eventsManager = $eventsManager;
    }

    /**
     * Returns the internal event manager
     *
     * @return ManagerInterface
     */
    public function getEventsManager()
    {
        return $this->_eventsManager;
    }

    public function onConstruct()
    {
        $this->services   = \Phalcon\Di::getDefault();

        if (MAINTENANCE && $this->dispatcher->getActionName() !== 'maintenance') {
            $this->response->redirect(array('for' => 'maintenance'));
            $this->response->send();

            exit;
        }

        if (!MAINTENANCE && APP_STATE !== APP_STATE_ALPHA) {
            $this->stylishApi = $this->services->get('Api_Stylish');
            $this->acl        = $this->services->get('Proxy_Acl')->getAcl();
        }
    }

    public function beforeExecuteRoute(\Phalcon\Mvc\Dispatcher $dispatcher)
    {
        if (!MAINTENANCE && APP_STATE !== APP_STATE_ALPHA) {
            $this->_checkPermissionsAndLoggedIn();
            $this->_checkIsAjax();
        }

        $config = $this->services->get('app_config');

        $this->_initializeTranslator($config, $dispatcher);
        $this->_setMeta();

        $this->_initGlobalVariables();
    }

    public function initialize()
    {

    }

    public function afterExecuteRoute()
    {
        // Don't want to add assets if the action gets forwarded.
        if (!$this->dispatcher->isFinished() || MAINTENANCE || APP_STATE === APP_STATE_ALPHA) {
            return;
        }

        $this->tag->setTitle($this->view->meta_title);

        $categoriesTree = $this->services->get('Proxy_Search')->getSearchFiltersSuggestions()['categories_tree'];
        $this->view->setVar('categoriesTree', $categoriesTree);
        // This variable is used in layout, for js translations
        $this->view->setVar('__translations', $this->_getJsTranslationsText());
        $this->view->setVar('__routes', $this->_getJsRoutesText());
        $this->view->setVar('__configs', $this->_getJsConfigs());
    }

    /**
     * @param \Phalcon\Config         $config
     * @param \Phalcon\Mvc\Dispatcher $dispatcher
     */
    private function _initializeTranslator($config, $dispatcher)
    {
        // Initialize the Translator
        $translatorManager = new TranslationsManager(
            array(
                'controller'			=> $dispatcher->getControllerName(),
                'action'				=> $dispatcher->getActionName(),
                'translationsDir'		=> $config->application->dirs->translationsDir,
                'lang'					=> $this->services->get('Proxy_Language')->getLanguage(),
                'customTranslations'	=> $this->services->get('Proxy_Translations')->getDbTranslations($this->services->get('Proxy_Language')->getLanguage())
            )
        );

        // Add translations manager, needed for javascript
        $this->services->setShared('translationsManager', function () use ($translatorManager) {
            return $translatorManager;
        });

        $translator = $translatorManager->getTranslator();

        // Assign the translator to the view
        $this->view->setVar('translate', $translator);

        // Add translator to service manager
        $this->services->setShared(
            'translator', function () use ($translator) {
            return $translator;
        }
        );
    }

    /**
     * Set meta tags
     */
    private function _setMeta()
    {
        $this->view->meta_title       = $this->services->get('translator')->_('seo.title');
        $this->view->meta_description = $this->services->get('translator')->_('seo.meta_description');
        $this->view->meta_keywords    = $this->services->get('translator')->_('seo.meta_keywords');
        $this->view->meta_image       = $this->services->get('app_config')->application->baseUri . 'img/logo.png';
        $this->view->meta_url         = $this->services->get('app_config')->application->baseUri . substr($this->router->getRewriteUri(), 1);
    }

    /**
     * Check if the user is logged in, and if it's not redirect to login page except some pages
     */
    private function _checkPermissionsAndLoggedIn()
    {
        $exceptActions = array(
            'login', 'loginUser', 'resetPassword', 'resetPasswordRequest', 'notFound',
            'permissionDenied', 'logout', 'register', 'registerUser', 'confirm_register', 'confirmRegister', 'landingPage'
        );

        $exceptControllers = array('beta', 'blog', 'impressum');

        if (
            !in_array($this->dispatcher->getActionName(), $exceptActions) &&
            !in_array(strtolower($this->dispatcher->getControllerName()), $exceptControllers) &&
            !$this->services->get('Proxy_Account')->isAdmin()
        ) {
            $resource = str_ireplace('Controller', '', get_class($this));

            if (
                !$this->acl->isAllowed($this->services->get('Proxy_Account')->getUserRoleId(), 'Frontend', 'All') &&
                !$this->acl->isAllowed(
                    $this->services->get('Proxy_Account')->getUserRoleId(),
                    $resource,
                    'All'
                ) &&
                !$this->acl->isAllowed(
                    $this->services->get('Proxy_Account')->getUserRoleId(),
                    $resource,
                    $this->dispatcher->getActionName()
                )
            ) {
                //                if (APP_ENV != 'development') {
                $this->dispatcher->forward($this->url->getAsArray(array('for' => URL_PERMISSION_DENIED)));
                //                }
            }
        }
    }

    /**
     * If it's an ajax call we do not need any layout or view and the response will always be JSON
     */
    private function _checkIsAjax()
    {
        if ($this->request->isAjax() == true) {
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
            $this->response->setContentType('application/json', 'UTF-8');
        }
    }

    /**
     * Get the Js translations text
     *
     * @return string
     */
    private function _getJsTranslationsText()
    {

        if (!$this->request->isAjax()) {
            $translationsManager = $this->services->get('translationsManager');
            $translations = $translationsManager->getTranslations();

            $text                = "
                <script type='text/javascript'>
                var tmpTranslations = " . json_encode($translations ) . ";
                if (typeof (__translations) !== 'undefined') {
                    $.extend(__translations, tmpTranslations);
                } else {
                    var __translations = tmpTranslations;
                }
                </script>
                ";

            return $text;
        }

        return '';
    }

    /**
     * Get the Js translations text
     *
     * @return string
     */
    private function _getJsRoutesText()
    {
        if (!$this->request->isAjax()) {
            $routes = $this->services->get('cache')->get('application_custom_routes_' . $this->services->get('Proxy_Language')->getLanguage());
            if (empty($routes)) {
                $routes = array();
            }
            $text = "
                <script type='text/javascript'>
                    var __routes = " . json_encode($routes) . ";
                </script>
                ";

            return $text;
        }

        return '';
    }

    /**
     * Get app configs and Constants for JS
     *
     * @return string
     */
    private function _getJsConfigs()
    {
        if (!$this->request->isAjax() && !MAINTENANCE && APP_STATE !== APP_STATE_ALPHA) {

            $config     = $this->services->get('app_config');
            $loggedUser = $this->services->get('session')->get('auth');

            unset($loggedUser['user_role_id']);
            unset($loggedUser['email']);

            $confingStr = "
    					<script type='text/javascript'>
    						var Api = {};
    						var Config = {};
    						document.domain = '" . $config->application->domain . "';
    						Api.url = '" . $config->api->stylish->url . "';
    						Api.port = '" . $config->api->stylish->port . "';
    						App.lu = " . json_encode($loggedUser) . ";
    						Config.app_env = '" . APP_ENV . "';
    						Config.lang = '" . LANGUAGE . "';

    					</script>
    				";
            return $confingStr;
        }

        return '';
    }

    /**
     * Initialize the global variables used in the application views
     */
    private function _initGlobalVariables()
    {
        if (!MAINTENANCE && APP_STATE !== APP_STATE_ALPHA) {
            // Use the logged user data everywhere
            $loggedUser     = $this->services->get('session')->get('auth');
            $this->view->setVar('loggedUser', $loggedUser);

            $this->view->setVar('currency', $this->services->get('Proxy_Currency')->getAllCurrencyData());
            $this->view->setVar('language', $this->services->get('Proxy_Language')->getAllLanguageData());
            $this->view->setVar('region', $this->services->get('Proxy_Region')->getAllRegionData());
            $this->view->setVar('socialLinks', $this->services->get('app_config')->social->pages->{$this->services->get('Proxy_Language')->getLanguage()});
        }

        $this->view->controllerName = $this->dispatcher->getControllerName();
        $this->view->actionName     = $this->dispatcher->getActionName();
        $translate = $this->services->get('translator');

    }
}
