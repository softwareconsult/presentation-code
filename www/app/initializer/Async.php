<?php

namespace App\Initializer;

class Async
{
	public static function enqueueOnCronServer($queue, $class, $args = array(), $subCategory = '')
    {
        $services = \Phalcon\Di::getDefault();

        $args['queue'] = $queue;
        $args['class'] = $class;
        $args['subCategory'] = $subCategory;

        return $services->get('Cron_Stylish')->get('async/enqueue', $args);
	}

    /**
     * If redis extension is loaded and redis workers have been started,create a new job and save it to the specified queue,
     * else run the job now and return 0
     *
     * @param        $queue
     * @param        $class
     * @param array  $args
     * @param string $subCategory
     * @param bool   $trackStatus - used for Resque logging
     *
     * @return int|string
     */
    public static function enqueue($queue, $class, $args = array(), $subCategory = '', $trackStatus = false)
    {
        // The list with the queues that do not process now if the workers not started
        $doNotProcessSyncQueuesList = array('crons_api', 'resize_images', 'amazon_cron_api');

        /* Check if workers have been started or we are in testing mode */
        if (
            extension_loaded('redis') &&                                                                    // check if the redis extension is loaded
            (in_array($queue, $doNotProcessSyncQueuesList) || self::queueIsInStartedWorker($queue, $class, $args, $subCategory))        // check if the queue is in a started worker or if we register anyway
        ) {
            if (!empty($subCategory)) {
                $class = "\\App\\Jobs\\" . ucfirst(strtolower($subCategory)) . "\\$class";
            } else {
                $class = "\\App\\Jobs\\$class";
            }

            $response = \Resque::enqueue($queue, $class, $args, $trackStatus);

            if (!empty($response)) {
                return $response;
            }
        }

        $job = self::_getJob($class, $subCategory);

        try {
            $job->process($args);
            $job->logger->info("The job " . $class . " has executed with success with arguments: " . print_r($args, true));
        } catch(\Exception $e) {
            $job->logger->error("The job " . $class . "has error with message: " . $e->getMessage() . " TRACE_ROUTE: " . print_r($e->getTraceAsString(), true));
        }

        // Means that the job is executed now, has no id
        return 0;

    }

    /**
     * Makes singletons for Jobs
     *
     * @param        $name
     * @param string $subCategory
     *
     * @return mixed
     */
    private static function _getJob($name, $subCategory = '')
    {
        $class = "\\App\\Jobs\\$name";

        if (!empty($subCategory)) {
            $class = "\\App\\Jobs\\$subCategory\\$name";
        }

        return new $class();
    }

    /**
     * Check if the queue is in a started worker
     *
     * @return bool
     */
    public static function queueIsInStartedWorker($queue, $class = '', $args =  array(), $subCategory = '')
    {
        if (APP_ENV == 'staging') {
            return true;
        }

        // Prune the dead workers if any
        $workers = \Resque_Worker::all();
        if (!empty($workers)) {
            foreach ($workers as $worker) {
                if (is_object($worker)) {
                    // we need to clear the dead workers and we only do it once because it does it for each worker
                    $worker->pruneDeadWorkers();
                    break;
                }
            }
        }

        $workers = \Resque_Worker::all();
        $activeQueues = array();
        if (!empty($workers)) {
            foreach ($workers as $worker) {
                if (is_object($worker)) {
                    $activeQueues = array_merge($activeQueues, $worker->queues());
                }
            }
        }

        $isActive = in_array($queue, $activeQueues);

        // if not active, send mail to developers
        if (!$isActive) {
            $message =
                'Queue: ' . $queue . PHP_EOL .
                'APP_ENV: '. APP_ENV . PHP_EOL .
                'Class: ' . $class . PHP_EOL .
                'Subcategory: ' . $subCategory . PHP_EOL .
                'Args: ' . print_r($args, true) . PHP_EOL;

            \Phalcon\Di::getDefault()->get('Proxy_Internal_Mails')->sendMailToDevelopers('Software-consult.ro[' . APP_ENV . '] Worker not started for queue ' . $queue, $message);
        }

        return $isActive;
    }
}