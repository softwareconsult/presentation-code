<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/27/14
 * Time: 12:18 PM
 */
namespace App\Initializer;

use Phalcon\Events\EventsAwareInterface;
use Phalcon\Events\Manager;
use Phalcon\Events\ManagerInterface;

abstract class BaseProxy implements EventsAwareInterface
{
    /**
     * @var \Phalcon\DI\FactoryDefault
     */
    protected $services = null;

    /**
     * @var \Phalcon\Events\Manager;
     */
    protected $eventsManager = null;

    /**
     * @var \App\Initializer\BaseModel;
     */
    protected $model = null;

    /**
     * @var \App\Initializer\BaseValidator
     */
    protected $validator = null;

    public function __construct()
    {
        $this->services = \Phalcon\Di::getDefault();
        $this->setEventsManager(new Manager());
        $this->initialize();
    }

    public function initialize() {}

    public function setEventsManager(ManagerInterface $eventsManager)
    {
        $this->eventsManager = $eventsManager;
    }

    public function getEventsManager()
    {
        return $this->eventsManager;
    }

    /**
     * @param \App\Initializer\BaseModel $model
     */
    public function setModel(\App\Initializer\BaseModel $model)
    {
        $this->model = $model;
    }

    /**
     * @return \App\Initializer\BaseModel
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param \App\Initializer\BaseValidator $validator
     */
    public function setValidator(\App\Initializer\BaseValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @return \App\Initializer\BaseValidator
     */
    public function getValidator()
    {
        return $this->validator;
    }
    
    /**
     * Created response answer
     * 
     * @param array $data
     * @param boolean $success
     * @return array
     */
    protected function response($data = null, $success = true)
    {
        $response['data'] = $data;
        $response['success'] = $success;
        return $response;     
    }
}