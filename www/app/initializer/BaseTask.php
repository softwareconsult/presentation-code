<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 9/18/14
 * Time: 2:18 PM
 */

namespace App\Initializer;

class BaseTask extends \Phalcon\CLI\Task
{
    /**
     * @var \Phalcon\DI\FactoryDefault
     */
    protected $services;

    public function __construct()
    {
        parent::__construct();
        $this->services = \Phalcon\Di::getDefault();
    }
}