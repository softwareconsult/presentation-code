<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 7/3/14
 * Time: 12:34 PM
 */
namespace App\Initializer;

abstract class BaseValidator
{
    /**
     *
     * @var \App\Validators\GeneralValidator
     */
    protected $generalValidator = null;

    protected $config;

    public function __construct()
    {
        $services = \Phalcon\Di::getDefault();
        $this->generalValidator = $services->get('GeneralValidator');
        $this->config = $services->get('app_config');
    }

    /**
     *
     * @param \Phalcon\Validation\Message\Group $response
     *
     * @return array
     */
    protected function toArray($response)
    {
        $processed = array();

        $response->rewind();
        while ($response->valid()) {
            $message = $response->current()->getMessage();
            if (!empty($message)) {
                $processed[$response->current()->getField()] = $message;
            }

            $response->next();
        }

        return $processed;
    }

    /**
     * Return the validation results
     *
     * @param \Phalcon\Validation $validator
     * @param array $params
     *
     * @return array|bool
     */
    protected function validate($validator, $params)
    {
        $messages = $validator->validate($params);

        if ($messages->count() === 0) {
            return true;
        }

        $messages = $this->toArray($messages);

        return $messages;
    }
}