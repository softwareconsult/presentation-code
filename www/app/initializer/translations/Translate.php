<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/26/14
 * Time: 4:38 PM
 */
namespace App\Initializer\Translations;

abstract class Translate
{
    /**
     * @var string
     */
    protected $controller = null;

    /**
     * @var string
     */
    protected $action = null;

    /**
     * @var string
     */
    protected $lang = null;

    /**
     * @var string
     */
    protected $translationsDir = null;

    public function __construct($options)
    {
        $this->setAction($options);
        $this->setController($options);
        $this->setLang($options);
        $this->setTranslationsDir($options);
    }

    /**
     * Retrieves the translations according to controller, action pair and the global ones
     */
    public function getTranslator() {}

    /**
     * @param string $options
     */
    protected function setAction($options)
    {
        if (isset($options['action'])) {
            $this->action = $options['action'];
        }
    }

    /**
     * @return null|string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $options
     */
    protected  function setController($options)
    {
        if (isset($options['controller'])) {
            $this->controller = $options['controller'];
        }
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param string $options
     */
    protected function setLang($options)
    {
        if (isset($options['lang'])) {
            $this->lang = $options['lang'];
        } else {
            $this->lang = LANGUAGE;
        }
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $options
     */
    protected function setTranslationsDir($options)
    {
        if (isset($options['translationsDir'])) {
            $this->translationsDir = $options['translationsDir'];
        } else {
            $this->translationsDir = APP_PATH . '/translations';
        }
    }

    /**
     * @return string
     */
    public function getTranslationsDir()
    {
        return $this->translationsDir;
    }

}