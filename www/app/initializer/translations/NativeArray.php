<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/26/14
 * Time: 4:40 PM
 */
namespace App\Initializer\Translations;

class NativeArray extends Translate
{

    /**
     * Retrieves the translations according to controller, action pair and the global ones
     * @return \Phalcon\Translate\Adapter\NativeArray
     */
    public function getTranslator()
    {
        $translations = array();

        $defaultPath = $this->translationsDir . DIRECTORY_SEPARATOR . $this->lang . '.php';
        if (file_exists($defaultPath)) {
            $tmpTranslation = include $defaultPath;
            $translations = array_merge($translations, $tmpTranslation);
        }

        if (!is_null($this->controller) && !is_null($this->action)) {
            $actionPath = $this->translationsDir .
                DIRECTORY_SEPARATOR .
                $this->controller .
                DIRECTORY_SEPARATOR .
                $this->action .
                DIRECTORY_SEPARATOR .
                $this->lang . '.php';
            if (file_exists($actionPath)) {
                $tmpTranslation = include $actionPath;
                $translations = array_merge($translations, $tmpTranslation);
            }

        }

        return new \Phalcon\Translate\Adapter\NativeArray(array(
            "content" => $translations
        ));
    }
}