<?php

/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/19/14
 * Time: 2:14 PM
 */

namespace App\Initializer;

use Library\Mails\PhpMailer;
use Phalcon\Logger\Adapter\File;

class ServiceManager
{
    /**
     * @param \Phalcon\DI\FactoryDefault $serviceManager
     *
     * @return \Phalcon\DI\FactoryDefault
     */
    public function registerServices($serviceManager)
    {

        $serviceManager->setShared('translator', function () use ($serviceManager) {
            // Initialize the Translator
            $translatorManager = new TranslationsManager(array(
                'translationsDir' => $serviceManager->get('app_config')->application->dirs->translationsDir,
                'lang' => $serviceManager->get('Proxy_Internal_Language')->getLanguage()
            ));

            return $translatorManager->getTranslator();

        });

		$serviceManager->setShared('Cron_Stylish', function() use ($serviceManager) {
            $obj =  new \App\Api\Stylish($serviceManager->get('app_config')->api->cron);
            $obj->setCrossServerAuth($serviceManager->get('crossServerAuth'));
            return $obj;
        });

        $this->_registerListeners($serviceManager);
        $this->_registerValidators($serviceManager);
        $this->_registerSanitizers($serviceManager);
        $this->_registerModels($serviceManager);
        $this->_registerProcessors($serviceManager);
        $this->_registerFrontendProxies($serviceManager);
        $this->_registerAdminProxies($serviceManager);
        $this->_registerInternalProxies($serviceManager);

        return $serviceManager;
    }

    /**
     * @param \Phalcon\DI\FactoryDefault $serviceManager
     */
    private function _registerListeners($serviceManager)
    {
        $listeners = array(
             'StyleAdvice'
        );

        foreach ($listeners as $listener) {
            $serviceManager->setShared(
                    'Listener_' . $listener, function () use ($listener) {
                $namespace = "\\App\\Listeners\\" . $listener;

                return new $namespace();
            }
            );
        }
    }

    /**
     * @param \Phalcon\DI\FactoryDefault $serviceManager
     */
    private function _registerValidators($serviceManager)
    {
        $serviceManager->setShared(
                'GeneralValidator', function () {
            return new \App\Validators\GeneralValidator();
        }
        );

        $validators = array(
            'StyleAdvice'
        );

        foreach ($validators as $validator) {
            $serviceManager->setShared(
                    'Validator_' . $validator, function () use ($validator) {
                $namespace = "\\App\\Validators\\" . $validator;

                return new $namespace();
            }
            );
        }
    }

    /**
     * @brief Define the sanitizers as services.
     *
     * @param \Phalcon\DI\FactoryDefault $serviceManager
     */
    private function _registerSanitizers($serviceManager)
    {
        $serviceManager->setShared('filter',  array(
            'className' => 'Phalcon\Filter'
        ));

        $sanitizers = array(
            'Comments', "Account","GeneralStringSanitizer"
        );

        foreach ($sanitizers as $sanitizer) {
            $serviceManager->setShared('Sanitizer_' . $sanitizer, array(
                'className' => 'App\\Sanitizers\\' . $sanitizer,
                'arguments' => array(
                    array('type' => 'service', 'name' => 'filter')
                )
            ));
        }
    }

    /**
     * @param \Phalcon\DI\FactoryDefault $serviceManager
     */
    private function _registerModels($serviceManager)
    {
        $models = array(
            'Acl', 'Account', 'StyleAdvice', 'Styles', 'Categories', 'Affiliates', 'Users', 'ApiPlatforms', 'Products',
            'ProductsImages', 'Currencies', 'ApiPlatformLogs', 'Banners', 'BannersZone',
            'Comments', 'Notifications', 'Newsletter', 'StaticPages', 'Countries', 'UserPoints',
            'Follows', 'Badges', 'ProductPrice', 'UserProfileDetails', 'SuggestStyle', 'Contests', 'Wishlists',
            'Search', 'Tags', 'UserPhotos', 'UserPhotosTaggedProducts', 'Amazon', 'Blog', 'Colors', 'Collages',
            'CustomCategories', 'CustomCategoriesItems', 'Likes', 'Gamification', 'Leaderboard', 'Vouchers',
            'ProductsCsv', 'ContestVotes', 'Routes', 'Currency', 'Language', 'Region', 'Translations', 'Gcm', 'Homepage',
        );

        foreach ($models as $model) {
            $serviceManager->setShared(
                    'Model_' . $model, function () use ($model) {
                $namespace = "\\App\\Models\\" . $model;

                return new $namespace();
            }
            );
        }
    }

    /**
     * @param \Phalcon\DI\FactoryDefault $serviceManager
     */
    private function _registerProcessors($serviceManager)
    {
        $serviceManager->setShared(
                'GeneralProcessor', function () {
            return new \App\Processors\GeneralProcessor();
        }
        );

        $processors = array(
            'Account', 'Acl', 'Search', 'Tags', 'Images', 'StaticPages', 'Amazon', 'Collages', 'Products', 'Tree',
            'Blog', 'Currency', 'Contests', 'Homepage', 'Banners', 'Notifications'
        );

        foreach ($processors as $processor) {
            $serviceManager->setShared(
                    'Processor_' . $processor, function () use ($processor) {
                $namespace = "\\App\\Processors\\" . $processor;

                return new $namespace();
            }
            );
        }
    }

    /**
     * @param \Phalcon\DI\FactoryDefault $serviceManager
     */
    private function _registerFrontendProxies($serviceManager)
    {
        $proxies = array(
            'Acl', 'Account', 'Styles', 'Categories', 'Products', 'Colors',
            'Users', 'Comments', 'Notifications', 'Newsletter', 'StaticPages', 'Countries', 'UserPoints',
            'Follows', 'Badges', 'Contests', 'Search', 'Wishlists', 'Tags', 'UserPhotos',
            'Blog', 'Collages', 'CustomCategories', 'Likes', 'Leaderboard', 'Banners', 'ContestVotes', 'Invite', 'Routes', 'Gcm',
            'Vouchers', 'Homepage', 'StyleAdvice', 'Currency', 'Language', 'Translations', 'Region',
        );

        foreach ($proxies as $proxyString) {;
            $serviceManager->setShared('Proxy_Frontend_' . $proxyString, function () use ($serviceManager, $proxyString) {
                $namespace = "\\App\\Proxies\\Frontend\\" . $proxyString;
                $proxy = new $namespace();

                // Set the common model
                if ($serviceManager->has('Model_' . $proxyString)) {
                    $proxy->setModel($serviceManager->get('Model_' . $proxyString));
                }

                // Set the common validator
                if ($serviceManager->has('Validator_' . $proxyString)) {
                    $proxy->setValidator($serviceManager->get('Validator_' . $proxyString));
                }

                // Set the listener
                if ($serviceManager->has('Listener_' . $proxyString)) {
                    $proxy->getEventsManager()
                            ->attach(strtolower($proxyString), $serviceManager->get('Listener_' . $proxyString));
                }

                return $proxy;
            }
            );
        }
    }

    /**
     * @param \Phalcon\DI\FactoryDefault $serviceManager
     */
    private function _registerAdminProxies($serviceManager)
    {
        $proxies = array(
            'Acl', 'Account', 'StyleAdvice', 'Styles', 'Categories', 'Affiliates', 'ApiPlatforms', 'Products',
            'ProductsImages', 'Currencies', 'Users', 'Banners', 'BannersZone',
            'Comments', 'Notifications', 'Newsletter', 'StaticPages', 'Countries', 'UserPoints',
            'Badges', 'SuggestStyle', 'UserPhotos',
            'Blog', 'Amazon', 'Colors', 'Collages', 'CustomCategories', 'Likes', 'Gamification', 'Vouchers',
            'Contests', 'ProductsCsv', 'Dashboard', 'Routes', 'Currency', 'Region', 'Language',
            'Homepage',
        );

        foreach ($proxies as $proxyString) {
            $serviceManager->setShared(
                'Proxy_Admin_' . $proxyString, function () use ($serviceManager, $proxyString) {
                    $namespace = "\\App\\Proxies\\Admin\\" . $proxyString;
                    $proxy = new $namespace();

                    // Set the common model
                    if ($serviceManager->has('Model_' . $proxyString)) {
                        $proxy->setModel($serviceManager->get('Model_' . $proxyString));
                    }

                    // Set the common validator
                    if ($serviceManager->has('Validator_' . $proxyString)) {
                        $proxy->setValidator($serviceManager->get('Validator_' . $proxyString));
                    }

                    // Set the listener
                    if ($serviceManager->has('Listener_' . $proxyString)) {
                        $proxy->getEventsManager()
                            ->attach(strtolower($proxyString), $serviceManager->get('Listener_' . $proxyString));
                    }

                    return $proxy;
                }
            );
        }
    }

    /**
     * @param \Phalcon\DI\FactoryDefault $serviceManager
     */
    private function _registerInternalProxies($serviceManager)
    {
        $proxies = array(
            'Categories', 'Search', 'Tags', 'Users', 'Products', 'Account', 'Likes', 'Collages', 'UserPhotos', 'Badges', 'Notifications',
            'Gamification', 'UserPoints', 'Newsletter', 'Acl', 'Comments', 'StaticPages', 'Routes', 'Gcm',  'Currency', 'Language', 'Translations',
            'Region',
        );

        foreach ($proxies as $proxyString) {
            $serviceManager->setShared(
                    'Proxy_Internal_' . $proxyString, function () use ($serviceManager, $proxyString) {
                $namespace = "\\App\\Proxies\\Internal\\" . $proxyString;
                $proxy = new $namespace();

                // Set the common model
                if ($serviceManager->has('Model_' . $proxyString)) {
                    $proxy->setModel(
                        $serviceManager->get('Model_' . $proxyString)
                    );
                }

                // Set the common validator
                if ($serviceManager->has('Validator_' . $proxyString)) {
                    $proxy->setValidator($serviceManager->get('Validator_' . $proxyString));
                }

                // Set the listener
                if ($serviceManager->has('Listener_' . $proxyString)) {
                    $proxy->getEventsManager()
                            ->attach(strtolower($proxyString), $serviceManager->get('Listener_' . $proxyString));
                }

                return $proxy;
            }
            );
        }

        $serviceManager->setShared(
                'Proxy_Internal_Mails', function () use ($serviceManager) {
            $proxy = new \App\Proxies\Internal\Mails();

            // Set the mail adapter
            $mailAdapter = new PhpMailer();
            $configs = $serviceManager->get('app_config');
            // Add mail server settings
            $mailAdapter->setSettings($configs->application->mails);
            // Add the logger
            $mailAdapter->setLogger($serviceManager->get('file')->create(APP_PATH . '/log/mails.error.log'));

            // Add the mail adapter to the logger
            $proxy->setMailAdapter($mailAdapter);

            $proxy->setTranslator($serviceManager->get('translator'));

            return $proxy;
        }
        );
    }
}
