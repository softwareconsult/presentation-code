<?php

class CallController extends \App\Initializer\BaseController
{
    /**
     * Interface between this api and others that requests data from here
     *
     * @return mixed
     */
    public function indexAction()
    {
        try {
            $allParams = json_decode($this->request->get('data'), true);

            $this->_validateParams($allParams);

            if (!$this->_hasPermissions($allParams['application'], $allParams['proxy'], $allParams['method'])) {
                if (APP_ENV !== 'development') {
                    return array('data' => 'permissionDenied', 'success' => false);
                }
            }

            return $this->_call($allParams);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Uploading a file and sending params
     *
     * @return bool
     */
    public function uploadAction()
    {
        try {
            $allParams = $this->request->get('data');

            if (is_string($allParams)) {
                $allParams = json_decode($allParams, true);
            }

            $this->_validateParams($allParams);

            if (!$this->_hasPermissions($allParams['application'], $allParams['proxy'], $allParams['method'])) {
                return array('data' => 'permissionDenied', 'success' => false);
            }

            // Get the form data
            if (!empty($allParams['form_data'])) {
                $params = json_decode($allParams['form_data'], true);
                unset($allParams['form_data']);
                $allParams = array_merge($allParams, $params);
            }

            if ($this->request->hasFiles() == true) {
                $allParams['files'] = $this->request->getUploadedFiles();
            }

            return $this->_call($allParams);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function embeddedAction($id = null)
    {
        if ($id !== null) {
            header("Access-Control-Allow-Origin: *");

            return $this->services->get('Proxy_Frontend_Collages')->getCollageEmbeddedData(array('id' => $id));
        }

        return null;
    }

    private function _call($allParams)
    {
        $application = ucfirst($allParams['application']);
        unset($allParams['application']);

        $proxy = ucfirst($allParams['proxy']);        // Store and unset the proxy param
        unset($allParams['proxy']);

        $method = $allParams['method'];               // Store and unset the method param
        unset($allParams['method']);

        // Check if the method will have parameters
        if (false === empty($allParams)) {
            $files = isset($allParams['files']) ? $allParams['files'] : null;
            /**
             * Apply sanitization to inputs if the service is defined for the current proxy.
             * Defaults all params to strings if no special mappings are provided.
             */
            if ($this->services->has('Sanitizer_' . $proxy)) {
                $allParams = $this->services->get('Sanitizer_' . $proxy)->sanitize($allParams);

            } else {
                $allParams = $this->services->get('Sanitizer_GeneralStringSanitizer')->sanitize($allParams);
            }

            if (null !== $files) {
                $allParams['files'] = $files;
            }

            return $this->services->get('Proxy_' . $application . '_' . $proxy)->$method($allParams);
        } else {
            return $this->services->get('Proxy_' . $application . '_' . $proxy)->$method();
        }
    }

    /**
     * @param $allParams
     */
    private function _validateParams($allParams)
    {
        if (empty($allParams['application']) || empty($allParams['proxy']) || empty($allParams['method'])) {
            throw new \RuntimeException('The application or proxy or method is not set!');
        }
    }

    /**
     * Check if the user has the permission to access
     *
     * @param $application
     * @param $proxy
     * @param $method
     *
     * @return bool
     */
    private function _hasPermissions($application, $proxy, $method)
    {
        $exceptActions = array(
            'login', 'isLogged', 'registerUser', 'forgotPassword', 'resetPassword', 'checkConfirmation',
            'getAcl', 'getActivePageByName'
        );

        $exceptFrontendResources = array('beta', 'blog');

        // Some actions can be processed no matter the acl and the admin has full rights
        if (
            in_array($method, $exceptActions) ||
            $this->services->get('Proxy_Internal_Account')->isAdmin() ||
            ($application == 'Frontend' && in_array(strtolower($proxy), $exceptFrontendResources))
        ) {
            return true;
        }

        $userRoleId = $this->services->get('Proxy_Internal_Account')->getUserRoleId();
        $acl        = $this->services->get('Proxy_Internal_Acl')->getAcl($application, false);

        // Check if it has access to whole application
        if ($acl->isAllowed($userRoleId, ucfirst($application), 'All')) {
            return true;
        }

        // Check if it has access to the specific method
        if ($acl->isAllowed($userRoleId, ucfirst($proxy), 'All') || $acl->isAllowed($userRoleId, ucfirst($proxy), $method)) {
            return true;
        }

        return false;
    }
}

