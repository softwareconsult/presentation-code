<?php
/**
 * Read constants
 */
include __DIR__ . "/config/constants.php";

if(APP_ENV!='production')
{
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}
else{
    //error_reporting(E_ALL);
    if(!DEBUG_MODE)
    ini_set('display_errors', 0);
}

// Set up Composer autoloader
require_once ROOT_PATH . '/vendor/autoload.php';

/**
 * Read the configuration
 */
$config = include APP_PATH . "/config/appconfig/config.php";
/**
 * Read auto-loader
 */
include APP_PATH . "/config/loader.php";

// Make sure that the application log folder is created
\Library\FileSystem\Folder::getInstance()->makeDir(APP_PATH, 'log/application');
// Register custom errors & exception handlers
$errorHandler = \Library\Error\ErrorsHandler::changeHandlers(
    array(
        'email'                 => $config->application->mails->developersEmail,
        'from'                  => 'Error Reporting <' . $config->application->mails->defaultReplyTo->email . '>',
        'debugMode'             => false,
        'sendMail'              => (APP_ENV == 'production'),
        'disabled'              => (APP_ENV == 'development'),
        'logPath'               => APP_PATH . '/log/application/',
        'executeDefaultHandler' => true,
        'app_env' => APP_ENV
    )
);

if (IS_JOB) {
    $errorHandler->setErrorsToHandle(array('E_ERROR', 'E_WARNING'));
}

// Set the time zone
date_default_timezone_set(DateTimeZone::listIdentifiers(DateTimeZone::UTC)[0]);

/**
 * Setup redis
 */
\Resque::setBackend($config->application->redis->hostName . $config->application->domainName . ':' . $config->application->redis->port);
\Resque::redis()->auth($config->application->redis->auth);
/**
 * Read services
 */
include APP_PATH . "/config/services.php";

/**
 * Set the headers
 */
$config  = $config->toArray();
$request = new \Phalcon\Http\Request();
$origin  = $request->getHeader('HTTP_ORIGIN');
if (in_array($origin, $config['application']['allowRequestsFrom'])) {
    header("Access-Control-Allow-Origin: " . $origin);
}
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Credentials: true');

return new \Phalcon\Mvc\Application($di);