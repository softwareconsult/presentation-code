<?php
$loader = new \Phalcon\Loader();
/**
 * We're a registering a set of directories taken from the configuration file
 */

//Register some namespaces
$loader->registerNamespaces(
    array(
        'App'                            => APP_PATH . '/',
        'App\Initializer'                => $config->application->dirs->initializerDir,
        'App\Models'                     => $config->application->dirs->modelsDir,
        'App\Proxies'                    => $config->application->dirs->proxiesDir,
        'App\Proxies\Internal'           => $config->application->dirs->proxiesDir . "/internal",
        'App\Proxies\Frontend'           => $config->application->dirs->proxiesDir . "/frontend",
        'App\Proxies\Admin'              => $config->application->dirs->proxiesDir . "/admin",
        'App\Api'                        => $config->application->dirs->apiDir,
        'App\Listeners'                  => $config->application->dirs->listenersDir,
        'App\Models\Tables'              => $config->application->dirs->modelsTablesDir,
        'App\Processors'                 => $config->application->dirs->processorsDir,
        'App\Validators'                 => $config->application->dirs->validatorsDir,
        'App\Sanitizers'                 => $config->application->dirs->sanitizersDir,
        'App\Initializer\Translations'   => $config->application->dirs->initializerDir . "/translations",
        'App\Jobs'                       => $config->application->dirs->jobsDir,
        'App\Jobs\Crons'                 => $config->application->dirs->jobsDir . "/crons",
        'App\Jobs\Search'                => $config->application->dirs->jobsDir . "/search",
        'App\Jobs\Update'                => $config->application->dirs->jobsDir . "/update",
        'App\Crons\Api'                  => $config->application->dirs->cronsDir . "/api",
        'App\Crons\Api\Processors'       => $config->application->dirs->cronsDir . "/api/processors",
        'App\Crons\Api\Mappers'          => $config->application->dirs->cronsDir . "/api/mappers",
        'App\Crons\Tasks'                => $config->application->dirs->cronsDir . "/tasks",
        'App\Crons\Tasks\UserDashboard'  => $config->application->dirs->cronsDir . "/tasks/user-dashboard",
        'App\Models\Products'            => $config->application->dirs->modelsDir . "/products",
        'App\Models\MongoCollections'    => $config->application->dirs->modelsDir . "/mongoCollections",
        'App\Models\Search'              => $config->application->dirs->modelsDir . "/search",
        'Phalcon\Session\Adapter'        => $config->application->dirs->libraryDir . "/Phalcon/Session/Adapter",
        'App\Exceptions'                 => $config->application->dirs->exceptionsDir,
        'App\ExternalImports'            => $config->application->dirs->externalImportsDir,
        'App\ExternalImports\Amazon'     => $config->application->dirs->externalImportsDir . '/amazon',
        'App\ExternalImports\Cj'         => $config->application->dirs->externalImportsDir . '/cj',
        'App\ExternalImports\ShareASale' => $config->application->dirs->externalImportsDir . '/share-a-sale',
        'App\ExternalImports\Zanox'      => $config->application->dirs->externalImportsDir . '/zanox',
        'App\Helper'                     => $config->application->dirs->helpersDir,
    )
);

$loader->registerDirs((array)$config->application->dirs);

$loader->register();