<?php

use Phalcon\Mvc\View;
use Library\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbMysqlPdo;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;

/**
 * The \Phalcon\DI\FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new \Phalcon\DI\FactoryDefault();

$di->setShared('collectionManager', function() {
    return new Phalcon\Mvc\Collection\Manager();
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('dbRead', function () use ($config) {
    $mySql = new DbMysqlPdo(array(
        'host' => $config->database->host,
        'port' => $config->database->read_port,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        'options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ));

    // I've done this to not estimate the responses for float values, check the function
    // \App\Models\Search:_getProductsDataForSearchQuery
    // and from what this says http://php.net/manual/en/pdo.setattribute.php it should not be an issue
//    $mySql->getInternalHandler()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

    if (APP_ENV !== 'staging' && APP_ENV !== 'production') {
        $mySql->getInternalHandler()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
    }

    return $mySql;
});

$di->setShared('dbWrite', function () use ($config) {
    $mySql = new DbMysqlPdo(array(
        'host' => $config->database->host,
        'port' => $config->database->write_port,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        'options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ));

    // I've done this to not estimate the responses for float values, check the function
    // \App\Models\Search:_getProductsDataForSearchQuery
    // and from what this says http://php.net/manual/en/pdo.setattribute.php it should not be an issue
    //    $mySql->getInternalHandler()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

    if (APP_ENV !== 'staging' && APP_ENV !== 'production') {
        $mySql->getInternalHandler()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
    }

    return $mySql;
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

$di->setShared('modelsManager', function() {
    return new Phalcon\Mvc\Model\Manager();
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () use ($config) {

    ini_set('session.save_handler', 'redis');
    ini_set('session.save_path', "tcp://" . $config->application->redis->hostName . $config->application->domainName .  ":" . $config->application->redis->port . "?weight=1&auth=" . $config->application->redis->auth);

    if (isset($config->application->session->name)) {
        ini_set('session.name', $config->application->session->name);
    }

    if (isset($config->application->session->lifetime)) {
        ini_set('session.gc_maxlifetime', $config->application->session->lifetime);
    }

    if (isset($config->application->session->cookie_lifetime)) {
        ini_set('session.cookie_lifetime', $config->application->session->cookie_lifetime);
    }

    if (isset($config->application->session->cookie_path)) {
        ini_set('session.cookie_path', $config->application->session->cookie_path);
    }

    if (isset($config->application->session->cookie_domain)) {
        ini_set('session.cookie_domain', $config->application->session->cookie_domain);
    }

    $session = new \Phalcon\Session\Adapter\Redis(array(
        "uniqueId" => "software-consult",
        "host" => $config->application->redis->hostName . $config->application->domainName,
        "port" => $config->application->redis->port,
        "auth" => $config->application->redis->auth,
        "name" => $config->application->session->name
    ));

    if (!$session->isStarted()) {
	    $session->start();
    }

    return $session;
});

/**
 * Add application configs to service manager
 */
$di->setShared('app_config', function() use ($config) {
    return $config;
});

/**
 * Setting up the view component for mails
 */
$di->setShared('view', function () use ($config) {
    $view = new View();
    $view->setViewsDir($config->application->dirs->viewsDir);
    $view->registerEngines(array(
	    '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
});

foreach ($config->application->domainNames as $domainName) {
    $serviceName = 'cache_' . $domainName;
    if ($domainName == $config->application->domainName) {
        $serviceName = 'cache';
    }

    $di->setShared($serviceName, function() use ($config, $domainName) {
        //Create a Data frontend and set a default lifetime to 10 year
        $frontend = new \Phalcon\Cache\Frontend\Data(array(
            'lifetime' => $config->application->redis->lifetime
        ));

        //Create the cache passing the connection
        $cache = new \Library\Cache\Backend\Redis($frontend, array(
            'host' => $config->application->redis->hostName . $domainName,
            'port' =>  $config->application->redis->port,
            'auth' => $config->application->redis->auth,
        ));

        return $cache;
    });

}

$di->setShared('mail', function() {
    // TODO: set the mail server
    return null;
});

$di->setShared('mailchimp', function() {
    // TODO: set the mail server
    return null;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared(
    'url', function () use ($config, $di) {
        $url = new UrlResolver();
        $baseUri = $config->application->appUri;
        // Add trailing slash if it doesn't have it
        if ($baseUri[strlen($baseUri) - 1] !== '/') {
            $baseUri .= '/';
        }
        $url->setBaseUri($baseUri);
        $url->setRouter($di->get('Proxy_Internal_Routes')->getRouter($di->get('Proxy_Internal_Language')->getLanguage()));
        return $url;
    }
);

/**
 * used to make requests to Google cloud messaging (GCM)
 */
$di->setShared(
    'gcm', function () use ($config, $di) {
    $config = $config->toArray();

    $obj = new \Library\Gcm\Gcm($config['gcm']);
    return $obj;
});

/**
 * Files handler
 */
$di->setShared('file', function () use ($config, $di) {
    $obj = new \Library\FileSystem\File();
    if(APP_ENV == 'production') {
        // Get settings
        $language = $di->get('Proxy_Internal_Language')->getLanguage();
        $currentBucket = $config['images']['amazonBuckets'][$language];
        $allBuckets = $config['images']['amazonBuckets'];
        $platformCredentials = $di->get('Model_ApiPlatforms')->getApiPlatformById(3, ['auth_key', 'secret_access_key']);

        // Initialize Amazon S3 adapter
        $imageAdapter = new \Library\FileSystem\ImageAdapters\AmazonImageAdapter(
            $currentBucket,
            $allBuckets,
            $platformCredentials['auth_key'],
            $platformCredentials['secret_access_key']
        );

        // Get from address
        $mailFrom = $di->get('app_config')->application->mails->defaultReplyTo->email;

        // Get developers email addresses
        $mailsList = $di->get('app_config')->application->mails->developersEmail;
        $mailsList = explode(',', $mailsList);
        array_walk($mailsList, 'trim');

        // Enable exception mailing for Amazon S3 adapter
        $imageAdapter->enableErrorMail($mailFrom, $mailsList);
    } else {
        $imageAdapter = new \Library\FileSystem\ImageAdapters\LocalAdapter(IMG_PATH);
		$cronImgAdapter = new \Library\FileSystem\ImageAdapters\CronAdapter();
		$obj->setCronImgAdapter($cronImgAdapter);
    }

    $obj->setImgAdapter($imageAdapter);

    return $obj;
});

$di->setShared('crossServerAuth', function () use ($config) {
    $config = $config->toArray();
    $obj = new \Library\Auth\CrossServerAuth($config['crossServer']);
    return $obj;
});

/**
 * Leave this at the end
 */
$serviceManager = new \App\Initializer\ServiceManager();
$di = $serviceManager->registerServices($di);

Phalcon\Di::setDefault($di);
