<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/19/14
 * Time: 1:44 PM

 */

// Define path to root directory
define('ROOT_PATH', realpath(__DIR__ . '/../..'));
// Define path to application directory
define('APP_PATH', realpath(ROOT_PATH . '/app'));

// Define application environment
if (!defined('APP_ENV')) {
    if (get_cfg_var('APP_ENV')) {
        define('APP_ENV', get_cfg_var('APP_ENV'));
    } else {
        define('APP_ENV', (getenv('APP_ENV') ? getenv('APP_ENV') : 'development'));
    }
}

define('FALLBACK_LANGUAGE', 'en');

// Whether or not command line interface
defined('IS_CLI') || define('IS_CLI', (getenv('IS_CLI') == 'true'));
// Whether or not is a job
defined('IS_JOB') || define('IS_JOB', (getenv('IS_JOB') == 'true'));

// Db timestamp format
define('TIMESTAMP_FORMAT', 'Y-m-d H:i:s');
// Db timestamp format
define('DATE_FORMAT', 'Y-m-d');
