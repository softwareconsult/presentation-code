<?php

include_once realpath(dirname(__DIR__) . '/../initializer/Settings.php');

// we need to process the settings so the production settings that are the same as the others will replace them
$settings = new \App\Initializer\Settings(array(
    'development' => include 'development.config.php',
    'dev_testing' => include 'dev_testing.config.php',
    'testing'     => include 'testing.config.php',
));

return new \Phalcon\Config($settings->getSettings());
