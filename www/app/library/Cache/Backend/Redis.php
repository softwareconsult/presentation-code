<?php
/*
  +------------------------------------------------------------------------+
  | Phalcon Framework                                                      |
  +------------------------------------------------------------------------+
  | Copyright (c) 2011-2012 Phalcon Team (http://www.phalconphp.com)       |
  +------------------------------------------------------------------------+
  | This source file is subject to the New BSD License that is bundled     |
  | with this package in the file docs/LICENSE.txt.                        |
  |                                                                        |
  | If you did not receive a copy of the license and are unable to         |
  | obtain it through the world-wide-web, please send an email             |
  | to license@phalconphp.com so we can send you a copy immediately.       |
  +------------------------------------------------------------------------+
  | Authors: Andres Gutierrez <andres@phalconphp.com>                      |
  |          Eduar Carvajal <eduar@phalconphp.com>                         |
  |          Nikita Vershinin <endeveit@gmail.com>                         |
  +------------------------------------------------------------------------+
*/
namespace Library\Cache\Backend;

/**
 * Phalcon\Cache\Backend\Redis
 * This backend uses redis as cache backend
 */
class Redis extends \Phalcon\Cache\Backend\Redis
{
    /**
     * Add element to list
     *
     * @param $listName
     * @param $keyName
     *
     * @return bool
     */
    public function listPush($listName, $keyName)
    {
        if($this->getRedis()->rPush($this->getPrefixedIdentifier($listName), $this->getPrefixedIdentifier($keyName)) === false) {
            return false;
        }

        return true;
    }

    /**
     * Get the content of the list
     *
     * @param $listName
     *
     * @return mixed
     */
    public function listGet($listName)
    {
        return $this->getRedis()->lRange($this->getPrefixedIdentifier($listName), 0, -1);
    }

    /**
     * @param string $identifier
     *
     * @return string
     */
    public function getPrefixedIdentifier($identifier)
    {
        if (!empty($this->_prefix)) {
            return $this->_prefix . $identifier;
        }

        return $identifier;
    }

    /**
     * Get php redis object
     *
     * @return \Redis
     */
    public function getRedis()
    {
        if (!$this->_redis) {
            $this->_connect();
        }

        return $this->_redis;
    }
}