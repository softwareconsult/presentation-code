<?php
/**
 * Used to compress and concatenate assets
 * User: software-consult.ro
 * Date: 2/26/2015
 * Time: 12:19 PM
 */

namespace Library\Assets;
use App\Exceptions\AssetsException;
use Phalcon\Assets\Manager as AssetsManager;

class Manager extends  AssetsManager
{

    /**
     * If we use versioning system
     *
     * @var bool
     */
    private $_useVersion = false;

    /**
     * The path where the assets are hold
     *
     * @var string
     */
    private $_assetsPath = '';

    protected $maps = array('js' => array(), 'css' => array());

    /**
     * @param array|null $options
     * send versionFile as options where to check if version number has changed
     */
    public function __construct($options=null)
    {
        parent::__construct($options);

        if(isset($options['useVersion']))
        {
            $this->_useVersion = $options['useVersion'];
        }

        if(!empty($options['assetsPath']) && is_string($options['assetsPath']))
        {
            // Add the trailing slash if it doesn't have it
            if ($options['assetsPath'][strlen($options['assetsPath']) - 1] !== '/') {
                $options['assetsPath'] .= '/';
            }

            $this->_assetsPath = $options['assetsPath'];
        }

        if (!empty($options['cssMap']))
        {
            $this->maps['css'] = $options['cssMap'];
        }

        if (!empty($options['jsMap']))
        {
            $this->maps['js'] = $options['jsMap'];
        }
    }

    /**
     * Prints the HTML for CSS resources
     *
     * @param string $collectionName
     * @param bool $join
     */
    public function outputCss($collectionName = null)
    {
        $this->_output($collectionName, 'css');
    }

    /**
     * Prints the HTML for CSS resources
     *
     * @param string $collectionName
     */
    public function outputJs($collectionName = null)
    {
        $this->_output($collectionName, 'js');
    }

    private function _output($collectionName, $type)
    {

        $isCdn = (strpos($collectionName, "_cdn") !== false);
        if (!empty($this->maps[$type][$collectionName])) {

            if (!$this->_useVersion && $isCdn) {
                return;
            }

            if ($this->_useVersion && !$isCdn && !empty($this->maps[$type][$collectionName . '_cdn'])) {
                return;
            }

            $collection = $this->collection($collectionName);

            if (!is_array($this->maps[$type][$collectionName])) {
                $this->maps[$type][$collectionName] = array($this->maps[$type][$collectionName]);
            }

            $prefix = '';
            if ($this->_useVersion && !$isCdn) {
                $prefix = ($type == 'css' ? 'stylesheets': $type) . '/production/';
            }

            foreach($this->maps[$type][$collectionName] as $item) {
                $collection->{'add' . ucfirst($type)}($prefix . $item);
            }

            if ($type == 'css') {
                parent::outputCss($collectionName);
            }

            if ($type == 'js') {
                parent::outputJs($collectionName);
            }
        }
    }
}