<?php
namespace Phalcon\Validation\Validator\Db;

use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;
use Phalcon\Validation\Message;
use Phalcon\Db\Adapter\Pdo as DbConnection;
use Phalcon\Validation\Exception as ValidationException;

/**
 * Phalcon\Validation\Validator\Db\Uniqueness

 * Validator for checking uniqueness of field in database

 * <code>
 * $uniqueness = new Uniqueness(
 *     array(
 *         'table' => 'users',
 *         'column' => 'login',
 *         'message' => 'already taken',
 *         'except'  => array (                 // Used at edit
 *              'column' => 'id',
 *              'value'  => '1'
 *          )
 *     ),
 *     $di->get('dbRead');
 * );
 *
 * </code>
 *
 * Or if you want to validate the uniqueness on multiple columns you can use use it like this
 *
 * <code>
 * $validation->add('name&login', new Uniqueness(array( // keep the same order
 *  'table' => 'users',
 *  'column' => array('username', 'login_token'),
 *  'message' => 'duplicate',
 *  'except'  => array (                 // Used at edit
 *              'column' => 'id',
 *              'value'  => '1'
 *          )
 * ), $di->get('dbRead')));
 * </code>

 * If second parameter will be null (omitted) than validator will try to get database
 * connection from default DI instance with \Phalcon\DI::getDefault()->get('db');
 */

class Uniqueness extends Validator implements ValidatorInterface
{
    /**
     * Database connection
     * @var \Phalcon\Db\Adapter\Pdo
     */
    private $db;

    public function __construct(array $options = array(), $db = null)
    {
        parent::__construct($options);

        if (null === $db) {
            // try to get db instance from default Dependency Injection
            $di = \Phalcon\DI::getDefault();

            if ($di instanceof \Phalcon\DiInterface && $di->has('dbRead')) {
                $db = $di->get('dbRead');
            }
        }

        if ($db instanceof DbConnection) {
            $this->db = $db;
        } else {
            throw new ValidationException('Validator Uniqueness require connection to database');
        }

        if (false === $this->hasOption('table')) {
            throw new ValidationException('Validator require table option to be set');
        }

        if (false === $this->hasOption('column')) {
            throw new ValidationException('Validator require column option to be set');
        }
    }

    /**
     * Executes the uniqueness validation
     *
     * @param  \Phalcon\Validation $validator
     * @param  string             $attribute
     * @return boolean
     */
    public function validate(\Phalcon\Validation $validator, $attribute)
    {
        $table = $this->getOption('table');
        $column = $this->getOption('column');

        // Check if we have multiple columns
        if (is_array($column)) {
            $sql = "SELECT COUNT(*) as count FROM `{$table}` WHERE ";
            $whereArr = array();
            foreach($column as $c) {
                $whereArr[] = "`{$c}` = ?";
            }

            $sql .= implode(' AND ', $whereArr);
        } else {
            $sql = "SELECT COUNT(*) as count FROM `{$table}` WHERE `{$column}` = ?";
        }

        // Check if we have multiple attributes to match with the columns
        $values = array();
        $attribute = explode('&', $attribute);
        foreach($attribute as $a) {
            $values[] = $validator->getValue($a);
        }

        // Check if we have an exception, if we want to skip this one record (used for edit)
        $except = $this->getOption('except');
        if (null !== $except) {
            $sql .= " AND `{$except['column']}` != ?";
            $values[] = $except['value'];
        }

        $result = $this->db->fetchOne(
            $sql,
            \Phalcon\Db::FETCH_ASSOC,
            $values
        );

        if ($result['count']) {
            $message = $this->getOption('message');

            if (null === $message) {
                $message = 'Already taken. Choose another!';
            }

            $validator->appendMessage(new Message($message, $attribute[0], 'Uniqueness'));

            return false;
        }

        return true;
    }
}