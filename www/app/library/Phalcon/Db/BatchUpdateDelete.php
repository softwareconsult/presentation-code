<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 1/14/2016
 * Time: 4:51 PM
 */

namespace Phalcon\Db;

class BatchUpdateDelete
{
    /**
     * @var \Phalcon\Db\Adapter\Pdo\Mysql
     */
    protected $writeConnection;

    /**
     * @var string
     */
    protected $table;

    protected $columnsTypes = array();

    protected $conditions = array();

    protected $bindParams = array();

    public function __construct($table = '', $writeConnection = null)
    {
        $this->table = $table;
        if (empty($writeConnection)) {
            if (\Phalcon\DI::getDefault()->has('dbWrite')) {
                $this->writeConnection = \Phalcon\DI::getDefault()->get('dbWrite');
            }
        } else {
            $this->writeConnection = $writeConnection;
        }
    }

    /**
     * Sets conditions for the query
     *
     * @param string $conditions
     * @param array $bindParams
     * @return $this
     */
    public function where($conditions, $bindParams=array())
    {
        $params = $this->_prepareConditions($conditions, $bindParams);
        $this->conditions = array($params['conditions']);
        $this->_setBindParams($params['bindParams'], false);
        return $this;
    }


    /**
     * Appends a condition to the current conditions using a AND operator
     *
     * @param string $conditions
     * @param array $bindParams
     * @return $this
     */
    public function andWhere($conditions, $bindParams=array())
    {
        $params = $this->_prepareConditions($conditions, $bindParams);
        $this->conditions = array_merge($this->conditions, $params['conditions']);
        $this->_setBindParams($params['bindParams'], true);
        return $this;
    }

    /**
     * Update the rows that match to the conditions
     *
     * @param array      $data        Required, $column => $value array
     * @param array      $whiteList   Optional, columns that can be updated
     * @param bool|false $debug       Optional, this is here to help you debug the queries
     *
     * @return bool|string
     * @throws \Exception
     */
    public function update($data, $whiteList = array(), $debug = false)
    {
        $this->_validate();

        if (empty($whiteList)) {
            $whiteList = array_keys($data);
        }

        $str = "UPDATE `{$this->table}` SET ";

        // Compose SET
        $setArr = array();
        $setBind = array();
        foreach($whiteList as $column) {
            if (isset($data[$column])) {
                $setArr[] = "`" . $column . "` = ?";
                $setBind[] = $data[$column];
            }
        }

        if (!empty($setArr)) {
            $str .= implode(', ', $setArr);
            $this->bindParams = array_merge($setBind, $this->bindParams);
        }

        $str .= $this->_getConditionsString();

        if ($debug) {
            return $str;
        }

        return $this->writeConnection->execute($str, $this->bindParams);
    }

    /**
     * Delete the rows
     *
     * @param bool|false $debug
     *
     * @return bool|string
     * @throws \Exception
     */
    public function delete($debug = false)
    {
        $this->_validate();
        $str = "DELETE FROM `{$this->table}`";

        $str .= $this->_getConditionsString();

        if ($debug) {
            return $str;
        }

        return $this->writeConnection->execute($str, $this->bindParams);
    }

    /**
     * @return Adapter\Pdo\Mysql
     */
    public function getWriteConnection()
    {
        return $this->writeConnection;
    }

    /**
     * @param Adapter\Pdo\Mysql $writeConnection
     *
     * @return $this
     */
    public function setWriteConnection($writeConnection)
    {
        $this->writeConnection = $writeConnection;
        return $this;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     *
     * @return $this
     */
    public function setTable($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return array
     */
    public function getBindParams()
    {
        return $this->bindParams;
    }

    /**
     * @param array $columnsTypes
     */
    public function setColumnsTypes($columnsTypes)
    {
        $this->columnsTypes = $columnsTypes;
    }

    /**
     * Seth the bind parameters and types
     *
     * @param array $bindParams
     * @param bool  $append
     */
    private function _setBindParams($bindParams=array(), $append = false)
    {
        if (!empty($bindParams) && is_array($bindParams)) {
            if ($append) {
                $this->bindParams = array_merge($this->bindParams, $bindParams);
            } else {
                $this->bindParams = $bindParams;
            }
        }
    }

    /**
     * Assemble the conditions into mysql string
     *
     * @return string
     */
    private function _getConditionsString()
    {
        return " WHERE " . implode(' AND ', $this->conditions);
    }

    /**
     * Prepare the conditions
     *
     * @param       $conditions
     * @param array $bindParams
     *
     * @return array
     */
    private function _prepareConditions($conditions, $bindParams = array())
    {
        $params = array(
            'conditions' => array(),
            'bind' => array()
        );

        // Check if the conditions is an array
        if (is_array($conditions)) {
            $tmpCondArr = array();
            foreach($conditions as $column => $value) {
                $tmpCondArr[] = "$column = ?";
                $params['bindParams'][] = $value;
            }

            if (!empty($tmpCondArr)) {
                $params['conditions'] = implode(' AND ', $tmpCondArr);
            }
        }

        // Check if the conditions is a string
        if (is_string($conditions)) {
            // CHeck if it has the form column = :bindName:
            if (strpos($conditions, ':') !== false) {
                foreach($bindParams as $name => $value) {
                    $conditions  = preg_replace("/:$name:/", "?", $conditions, 1);
                    $params['bindParams'][] = $value;
                }
                $params['conditions'] = $conditions;
            } else {
                $params['conditions'] = $conditions;
                $params['bindParams'] = $bindParams;
            }
        }

        return $params;
    }

    /**
     * Validates the data before calling SQL
     *
     * @throws \Exception
     */
    private function _validate()
    {
        if (empty($this->table)) {
            throw new \Exception('BatchUpdateDelete table must be defined');
        }

        if (empty($this->conditions)) {
            throw new \Exception('BatchUpdateDelete conditions must be added');
        }
    }
}