<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 8/22/14
 * Time: 11:48 AM
 * Allows batch inserts
 *
 * @usage
 *         $batch = new BatchInsert('users', $db);
 *         $batch->columns = array('id', 'name');
 *         $values = array(
 *         array(1, 'john'),
 *         array(4, 'fred'),
 *         array(3, 'mickey'),
 *         );
 *         $batch->setValues($values)->insert();

 */
namespace Phalcon\Db;

class BatchInsert
{
    /**
     * @var string
     */
    public $table = null;

    /**
     * @var array
     */
    public $columns = array();

    /**
     * @var array
     */
    protected $values = array();

    /**
     * @var null|\Phalcon\Db\AdapterInterface
     */
    protected $db = null;

    /**
     * @var string
     */
    private $_columnsString = '';

    /**
     * @var string
     */
    private $_bindString = '';

    /**
     * @var array
     */
    private $_valuesFlattened = array();

    /**
     * @param bool|string                       $table
     * @param null|\Phalcon\Db\AdapterInterface $db
     */
    public function __construct($table = false, $db = null)
    {
        if ($table) {
            $this->table = (string)$table;
        }

        if (is_null($db)) {
            $this->db = \Phalcon\DI::getDefault()->get('dbWrite');
        } else {
            $this->db = $db;
        }

        return $this;
    }

    /**
     * @param \Phalcon\Db\AdapterInterface $db
     */
    protected function setDbAdapter($db)
    {
        $this->db = $db;
    }

    /**
     * Set the Columns
     *
     * @param array $columns
     *
     * @return object Batch
     */
    public function setColumns($columns)
    {
        $this->columns        = $columns;
        $this->_columnsString = sprintf('`%s`', implode('`,`', $this->columns));

        return $this;
    }

    /**
     * Set the values
     *
     * @param $values
     *
     * @return $this
     * @throws \Exception
     */
    public function setValues($values)
    {
        if (!$this->columns) {
            throw new \Exception('You must setColumns() before setValues');
        }
        $this->values = $values;

        $valueCount = count($values);
        $fieldCount = count($this->columns);

        // Build the Placeholder String
        $placeholders = [];
        for ($i = 0; $i < $valueCount; $i++) {
            $placeholders[] = '(' . rtrim(str_repeat('?,', $fieldCount), ',') . ')';
        }
        $this->_bindString = implode(',', $placeholders);

        // Build the Flat Value Array
        $valueList = array();

        foreach ($values as $value) {
            if (is_array($value)) {
                foreach ($value as $v) {
                    $valueList[] = $v;
                }
            } else {
                $valueList[] = $values;
                break;
            }
        }

        $this->_valuesFlattened = $valueList;
        unset($valueList);

        return $this;
    }

    /**
     * Insert into the Database
     *
     * @param boolean $ignore Use an INSERT IGNORE (Default: false)
     * @param bool    $debug
     *
     * @return bool
     * @throws \Exception
     */
    public function insert($ignore = false, $debug = false)
    {
        $this->_validate();

        // Optional ignore string
        if ($ignore) {
            $insertString = "INSERT IGNORE INTO `%s` (%s) VALUES %s";
        } else {
            $insertString = "INSERT INTO `%s` (%s) VALUES %s";
        }

        $query = sprintf(
            $insertString,
            $this->table,
            $this->_columnsString,
            $this->_bindString
        );
        if ($debug) {
            return $query;
        }

        return $this->db->execute($query, $this->_valuesFlattened);
    }

    /**
     * Validates the data before calling SQL
     *
     * @throws \Exception
     */
    private function _validate()
    {
        if (!$this->table) {
            throw new \Exception('Batch Table must be defined');
        }

        $requiredCount = count($this->columns);

        if ($requiredCount == 0) {
            throw new \Exception('Batch Columns cannot be empty');
        }

        foreach ($this->values as $value) {
            if (count($value) !== $requiredCount) {
                throw new \Exception('Batch Values must match the same column count of ' . $requiredCount);
            }
        }
    }
}