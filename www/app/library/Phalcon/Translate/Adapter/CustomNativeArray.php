<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 11/28/2014
 * Time: 12:15 PM
 */
namespace Phalcon\Translate\Adapter;

use Phalcon\Exception;

class CustomNativeArray extends NativeArray
{

    /**
     * Returns the translation string of the given key, throws exception if do not exist
     *
     * @param string $translateKey
     * @param array  $placeholders
	 * @param bool   $strict
     *
     * @return string
     */
    public function _($translateKey, $placeholders = null, $strict = true)
    {
		$translatedString = parent::_($translateKey, $placeholders);

        if (
            $translatedString == $translateKey &&
            $strict &&
            defined('APP_ENV') &&
            APP_ENV !== 'production' &&
            APP_ENV !== 'staging'
        ) {
//            try{
//                throw new Exception("Aici");
//            } catch(\Exception $e) {
//                print "<pre>" . ($e->getTraceAsString()) . "</pre>";
//            }
            echo '<script type="text/javascript"> alert(\'' . 'There is no translation for key: ' . $translateKey . '\');</script>'; die;
        }

        $translatedString =  preg_replace('/(%([a-zA-Z_]+)%)/i', '', $translatedString);

        return $translatedString;
    }
} 