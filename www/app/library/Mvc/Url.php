<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/17/2015
 * Time: 2:59 PM
 */
namespace Library\Mvc;

class Url extends \Phalcon\Mvc\Url
{
    /**
     * Generates a URL
     *
     *<code>
     *
     * //Generate a URL appending the URI to the base URI
     * echo $url->get(array('for' => 'products/edit'));
     *
     * //Generate a URL appending the URI to the base URI and having params
     * echo $url->get(array('for' => 'products/edit', 'params' => array(1)));
     *
     * //Generate a URL for a predefined route
     * echo $url->get(array('for' => 'blog-post', 'title' => 'some-cool-stuff', 'year' => '2012'));
     *
     *</code>
     *
     * @param string|array $uri
     * @param array|object args Optional arguments to be appended to the query string
     * @param bool|null $local
     * @return string
     *
     */
    public function get($uri=null, $args=null, $local=null)
    {
        if (is_array($uri) && !empty($uri['for'])) {
            if (!$this->_router->getRouteByName($uri['for'])) {
                return $this->_addParams(parent::get($uri['for'], $args, $local), $uri);
            }
        }
        if (!empty($uri['params']) && is_array($uri['params'])) {
            $uri['params'] = array_filter($uri['params']);
            $uri['params'] = implode('/', $uri['params']);
        }

        return $this->_removeTrailingSlash(parent::get($uri, $args, $local));
    }

    /**
     * Get the routes parameters, including controller and action
     *
     * @param null $uri
     *
     * @return array
     */
    public function getAsArray($uri=null)
    {
        $array = array();
        $route = $uri;

        if (is_array($uri) && !empty($uri['for'])) {
            $route = $uri['for'];
            unset($uri['for']);
        }

        if (!empty($route)) {
            $routeObj = $this->_router->getRouteByName($route);
            // If not a custom route
            if (!$routeObj) {
                $parts = explode('/', $route);
                // set the controller;
                $array['controller'] = $parts[0];
                unset($parts[0]);

                if (count($parts) == 0) {
                    $array['action'] = 'index';
                } else {
                    $array['action'] = $parts[1];
                }
            } else {
                $array = $routeObj->getPaths();
            }
        }

        if (is_array($uri) && !empty($uri)) {
            $array = array_merge($array, $uri);
        }

        return $array;
    }

    /**
     * Add params to routes that have :params at the end of the route
     *
     * @param $url
     * @param $urlParams
     *
     * @return string
     */
    private function _addParams($url, $urlParams)
    {
        if (!empty($urlParams) && !empty($urlParams['params'])) {

            $paramsParsed = $urlParams['params'];
            if (is_array($paramsParsed)) {
                $paramsParsed = implode('/', $urlParams['params']);
            }

            // Check if the route has any query parameters
            if (strpos($url, '?') === false) {
                return $this->_addTrailingSlash($url) . $paramsParsed;
            } else {
                $parts = explode('?', $url);
                $parts[0] = $this->_addTrailingSlash($parts[0]);
                $parts[0] .= $paramsParsed;
                return implode('?', $parts);
            }
        }

        return $url;
    }

    /**
     * Add the trailing slash to the url
     *
     * @param $url
     *
     * @return string
     */
    private function _addTrailingSlash($url)
    {
        if ($url[strlen($url) -1] !== '/') {
            $url .= '/';
        }

        return $url;
    }

    /**
     * Remove the trailing slash
     *
     * @param $url
     *
     * @return string
     */
    private function _removeTrailingSlash($url)
    {
        if ($url[strlen($url) - 1] == '/') {
            $url = substr($url, 0, -1);
        }

        return $url;
    }

    /**
     * @param mixed $router
     */
    public function setRouter($router)
    {
        $this->_router = $router;
    }

    /**
     * @return mixed
     */
    public function getRouter()
    {
        return $this->_router;
    }
}