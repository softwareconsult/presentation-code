<?php

namespace App\Models\Tables;

class StyleAdvice extends \App\Initializer\BaseTable
{
	/**
	 * Mapping db table columns
	 */
    public $id;
    public $style_advisor_id;
    public $email;
	public $name;
    public $question;
	public $response;
	public $status = STATUS_INACTIVE;
	public $user_id;
	public $observations;
    public $created_time;
	public $last_update;

    public function beforeSave()
    {
        $this->last_update = date(TIMESTAMP_FORMAT);
    }

    public function beforeCreate()
    {
        if (!isset($this->created_time)) {
            $this->created_time = date(TIMESTAMP_FORMAT);
        }
        $this->last_update = date(TIMESTAMP_FORMAT);
    }

	public function beforeUpdate()
    {
        $this->last_update = date(TIMESTAMP_FORMAT);
    }

    public function getErrors()
    {
        return $this->_errorMessages;
    }
}
