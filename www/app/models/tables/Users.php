<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 7/1/14
 * Time: 10:57 AM
 */

namespace App\Models\Tables;

class Users extends \App\Initializer\BaseTable
{
    public $id;

    public $user_role_id = 2;

    public $username;

    public $email;

    public $password;

    public $status = 0;

    public $register_date;
    
    public $request_reset_password = null;

    public $salt;

    public $remember_me_token = null;

    public $remember_me_expire_time = null;

    public function beforeCreate()
    {
        if (!isset($this->register_date)) {
            $this->register_date = date(TIMESTAMP_FORMAT);
        }
    }
}