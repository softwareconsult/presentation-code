<?php


namespace App\Models;

use App\Initializer\BaseModel;
use App\Models\Tables\StyleAdvice as StyleAdviceTable;
use App\Models\Tables\Users as UsersTable;
use Phalcon\Db\BatchInsert;
use Phalcon\Exception;

class StyleAdvice extends BaseModel
{
    const FASHION_ADVISORS_LIMIT = 5;
    
    /**
     * Get a list of users assigned as fashion advisors
     * 
     * @return array
     */
    public function getActiveFashionAdvisors()
    {
        $limit = self::FASHION_ADVISORS_LIMIT;
        $query = $this->modelsManager
            ->createBuilder()
            ->columns(array('u.username', 'u.user_role_id', 'u.id', 'ud.avatar'))
            ->addFrom('\App\Models\Tables\Users', 'u')
            ->leftJoin('\App\Models\Tables\UserDetails', 'ud.user_id = u.id', 'ud')
            ->where("u.status = " . STATUS_ACTIVE . " AND " . "u.user_role_id = " . FASHION_ADVISOR_ROLE_ID)
            ->orderBy('u.id ASC')
            ->limit($limit);

        return $query->getQuery()->execute()->toArray();
    }

    /**
     * Get style advices filtered
     *
     * @param array      $filters
     * @param bool|false $getCount
     *
     * @return int|array
     */
    public function getStyleAdviceFiltered($filters = array(), $getCount = false)
    {
        if ($getCount) {
            $columns = array('COUNT(DISTINCT(sa.id)) as count');
        } else {
            /* define columns for results query */
            $columns = array(
                'sa.id',
                'sa.style_advisor_id',
                'sa.email',
                'sa.question',
                'sa.response',
                'sa.observations',
                'sa.status',
                'sa.created_time',
                'sa.last_update',
                'sa.name',
                'sa.user_id',
                'u.username AS advisor'
            );
        }

        $query = $this->modelsManager
            ->createBuilder()
            ->columns($columns)
            ->addFrom('\App\Models\Tables\StyleAdvice', 'sa')
            ->leftJoin('\App\Models\Tables\Users', 'sa.style_advisor_id = u.id', 'u');

        if (isset($filters['status'])) {
            if (is_array($filters['status'])) {
                $query->andWhere($this->escapeWhereInCondition('sa.status', $filters['status']));
            } else {
                $query->andWhere("sa.status = :theStatus:", array('theStatus' => $filters['status']));
            }
        }

        if (isset($filters['style_advisor_id'])) {
            $query->andWhere("sa.style_advisor_id = :styleAdviserId:", array("styleAdviserId" => $filters['style_advisor_id']));
        }

        if (isset($filters['email'])) {
            $query->andWhere("sa.email LIKE :theEmail:", array("theEmail" => "'%" . $filters['email'] . "%'"));
        }

        if (isset($filters['name'])) {
            $query->andWhere("sa.name LIKE :theName:", array("theName" => "'%" . $filters['name'] . "%'"));
        }

        if (isset($filters['question'])) {
            $query->andWhere("sa.question LIKE :theQuestion:", array("theQuestion" => "'%" . $filters['question'] . "%'"));
        }

        if (isset($filters['response'])) {
            $query->andWhere("sa.response LIKE :theResponse:", array("theResponse" => "'%" . $filters['response'] . "%'"));
        }

        if (isset($filters['observations'])) {
            $query->andWhere("sa.observations LIKE :theObservations:", array("theObservations" => "'%" . $filters['observations'] . "%'"));
        }

        if (isset($filters['advisor'])) {
            $query->andWhere("u.username LIKE :theAdvisor:", array("theAdvisor" => "'%" . $filters['advisor'] . "%'"));
        }

        if (isset($filters['limit']) && !$getCount) {
            $query->limit($filters['limit'], (isset($filters['offset']) ? intval($filters['offset']) : 0));
        }

        $query->orderBy('sa.last_update DESC');

        if (!$getCount) {
            $query->groupBy('sa.id');
        }

        $result = $query->getQuery()->execute()->toArray();

        if ($getCount) {
            return (int)$result[0]['count'];
        }

        return $result;
    }

    /**
     * Saves the fashion advice request to the database
     * 
     * @param array $params
     * @return array
     */
    public function insertStyleAdvice($params, $whiteList = array())
    {
        $styleAdviceModel = new StyleAdviceTable();
        if (empty($whiteList)) {
            $whiteList = array_keys($params);
        }

        return $styleAdviceModel->save($params, $whiteList);
    }

    /**
     * @param       $params
     * @param array $whiteList
     *
     * @return array|bool
     */
    public function updateStyleAdvice($params, $whiteList = array())
    {
        $id = (int) $params['id'];
        $styleAdvice = StyleAdviceTable::findFirst("id=$id");
        if ($styleAdvice) {
            $old = $styleAdvice->toArray();
            if (empty($whiteList)) {
                $whiteList = array_keys($params);
            }

            if ($styleAdvice->update($params, $whiteList)) {
                return $old;
            }
        }

        return false;
    }

}
