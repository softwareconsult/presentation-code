<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 12/2/2015
 * Time: 5:07 PM
 */

namespace App\Validators;

use App\Initializer\BaseValidator;
use Phalcon\Validation\Validator\Email;

class StyleAdvice extends BaseValidator
{
    public function validateRequestAdviceParams($params)
    {
        $validation = new \Phalcon\Validation();

        $validation = $this->generalValidator->addFieldRequiredValidation($validation, 'name');

        $validation->add('email', new Email(array(
            'message' => 'invalidEmail'
        )));

        $validation = $this->generalValidator->addFieldRequiredValidation($validation, 'question');
        $validation = $this->generalValidator->addFieldRequiredValidation($validation, 'style_advisor_id');

        return $this->validate($validation, $params);
    }
}