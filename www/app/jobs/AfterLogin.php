<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 9/5/14
 * Time: 10:49 AM
 */

namespace App\Jobs;

use App\Initializer\BaseJob;

class AfterLogin extends BaseJob
{
    public function process($params = null)
    {
        if (isset($params['rememberMe']) && $params['rememberMe']) {
            if (!empty($params['remember_me_expire_time']) && is_numeric($params['remember_me_expire_time'])) {
                $params['remember_me_expire_time'] = date(TIMESTAMP_FORMAT, $params['remember_me_expire_time']);
            }

            $this->services->get('Model_Account')->updateUserAccountData($params['email'], $params, array('remember_me_token', 'remember_me_expire_time'));
        }
    }
}