<?php
/**
 * Created by software-consult.ro.
 * Email: contact@software-consult.ro
 * Date: 6/16/2015
 * Time: 1:45 PM
 */

namespace App\Jobs;


use App\Initializer\BaseJob;

class ProcessProxyMethod  extends BaseJob
{
    public function process($params = null)
    {
        if (!empty($params)&& !empty($params['proxy']) && !empty($params['method']))
        {
            $proxy = $params['proxy'];
            unset($params['proxy']);
            $method = $params['method'];
            unset($params['proxy']);

            $application = 'Internal';
            if (!empty($params['application'])) {
                $application = ucfirst(strtolower($params['application']));
                unset($params['application']);
            }

            if (!empty($params)) {
                $this->services->get('Proxy_' . $application . '_' . $proxy)->$method($params);
            } else {
                $this->services->get('Proxy_' . $application . '_' . $proxy)->$method();
            }
        }
    }
} 