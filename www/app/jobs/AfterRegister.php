<?php
namespace App\Jobs;

use App\Initializer\BaseJob;

class AfterRegister extends BaseJob
{
    public function process($params = null)
    {
        $job = new \App\Jobs\GenerateSecurityToken();
        $job->process($params);

        $this->services->get('Proxy_Internal_Mails')->sendRegisterValidationEmail($params);
    }
}