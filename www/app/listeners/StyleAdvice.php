<?php

namespace App\Listeners;
use App\Initializer\Async;
use \App\Initializer\BaseListener;

class StyleAdvice extends BaseListener
{
    /**
     * Listen for style advice request
     *
     * @param \Phalcon\Events\Event Object $event
     */
    public function afterRequestFashionAdvice($event)
    {
        $data = $event->getData();

        $mail = array(
            'proxy'  => 'Mails',
            'method' => 'sendStyleAdviceInitiatedToAdmin'
        );
        Async::enqueue('mails', 'ProcessProxyMethod', array_merge($mail, $data));
    }

    /**
     * After a style advice was updated
     *
     * @param $event
     */
    public function afterUpdateStyleAdvice($event)
    {
        $data = $event->getData();
        $old = $data['old'];
        $new = $data['new'];

        // Detect status changed
        if ($old['status'] != $new['status']) {
            // When a style advice get's active announce the style advisor
            if ($new['status'] == STATUS_ACTIVE) {
                $mail = array(
                    'proxy'  => 'Mails',
                    'method' => 'sendStyleAdviceActivated'
                );
                Async::enqueue('mails', 'ProcessProxyMethod', array_merge($mail, $new));
            }

            // If the style advice get's answered by style advisor let the admin know
            if ($new['status'] == STATUS_ANSWERED) {
                $mail = array(
                    'proxy'  => 'Mails',
                    'method' => 'sendStyleAdviceReplyToAdmin'
                );
                Async::enqueue('mails', 'ProcessProxyMethod', array_merge($mail, $new));
            }

            // If the admin validates the response than announce the user that asked the question
            if ($new['status'] == STATUS_VALIDATED) {
                $mail = array(
                    'proxy'  => 'Mails',
                    'method' => 'sendStyleAdviceReplyToUser'
                );
                Async::enqueue('mails', 'ProcessProxyMethod', array_merge($mail, $new));

                if (!empty($new['user_id'])) {
                    // Notify user through the internal notification system
                    $notification = array(
                        'proxy' => 'Notifications',
                        'method' => 'notifyUserAfterStyleAdviceReply',
                    );

                    Async::enqueue('default', 'ProcessProxyMethod', array_merge($notification, $new));
                }
            }
        }
    }

}
