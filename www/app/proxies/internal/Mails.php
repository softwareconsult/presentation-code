<?php

namespace App\Proxies\Internal;

use App\Initializer\BaseProxy;
use App\Exceptions\MailsException;

/**
 * Class Mails
 *
 * @package App\Proxies\Internal
 */
class Mails extends BaseProxy
{
    /**
     * @var \Phalcon\Mvc\View
     */
    protected $mailsView = null;

    /**
     * @var \Library\Mails\PhpMailer
     */
    protected $mailAdapter = null;

    /**
     * @var mixed
     */
    protected $translator = null;

    /**
     * Initialize mails view
     */
    public function __construct()
    {
        parent::__construct();

        $this->mailsView = $this->services->get('view');

        // Set the default layout script
        $this->mailsView->setLayout('mails');
        $this->mailsView->baseUri = $this->services->get('app_config')->application->baseUri;
        $this->mailsView->url = $this->services->get('url');
    }

    /**
     * Set the mail sending adapter
     *
     * @param mixed $mailAdapter
     */
    public function setMailAdapter($mailAdapter)
    {
        $this->mailAdapter = $mailAdapter;
    }

    /**
     * Set the mails view translator
     *
     * @param mixed $translator
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
        $this->mailsView->setVar('translate', $translator);
    }

    /**
     * Sends an email
     *
     * @param strign $to
     * @param string $from
     * @param string $subject
     * @param string $message
     * @param string $replyTo
     * @param null   $cc
     * @param null   $bcc
     *
     * @throws MailsException
     */
    public function send($to, $subject, $message = '', $from = null, $replyTo = null, $cc = null, $bcc = null)
    {
        if (empty($to) || empty($subject)) {
            throw new MailsException("Invalid parameters supplied for mail");
        }

        $this->mailAdapter->send($to, $subject, $message, $from, $replyTo, $cc, $bcc);
    }

    /**
     * Mail sent when user requests a forgot password mail
     *
     * @param array $params
     */
    public function sendForgotPasswordEmail($params)
    {
        $this->_unsubscribeNewsletter($params['email']);

        $view = clone $this->mailsView;

        $currentDate = date(TIMESTAMP_FORMAT);

        $data = $this->services->get('Processor_Account')->generateToken(
            array($params['id'], $params['email'], $currentDate)
        );

        $this->services->get('Model_Account')
            ->updateUser($params['id'], array('request_reset_password' => $currentDate));

        $mailData = array(
            'params' => $params,
            'token'  => $data
        );

        $view->setVars($mailData);
        $message = $view->getRender('mails', 'forgotPassword');

        $this->send($params['email'], $this->translator->_('forgotPassword.title'), $message);
    }

    /**
     * Send user activation account email
     *
     * @param $params
     *
     * @return bool
     * @throws MailsException
     */
    public function sendRegisterValidationEmail($params)
    {
        if (isset($params['user_id'])) {
            $params = $this->services->get('Model_Users')->getUserById(
                $params['user_id'], array('id', 'email', 'username', 'register_date', 'status', 'salt')
            );
            if (empty($params)) {
                return false;
            }
        } else {
            return false;
        }

        $validateToken = $this->services->get('Processor_Account')->generateToken(array(
                $params['id'],
                $params['email'],
                $params['username'],
                $params['register_date'],
                $params['status'],
                $params['salt']
            ));

        $this->_unsubscribeNewsletter($params['email']);

        $view         = clone $this->mailsView;
        $view->params = array(
            'username'       => $params['username'],
            'teamEmail'      => $this->services->get('app_config')->application->mails->defaultReplyTo->email,
            'user'           => $params,
        );

        $view->setVar('token', $validateToken);

        $message = $view->getRender('mails', 'registerValidation');
        $title   = $this->translator->_('registerValidation.title');

        $this->send($params['email'], $title, $message);

        return true;
    }


    /**
     * Send style advice activated mail to advisor
     * 
     * @param array $params
     */
    public function sendStyleAdviceActivated($params)
    {
        $view = clone $this->mailsView;
        $advisor = $this->services->get('Model_Users')->getUserById($params['style_advisor_id'], 'email');
        $view->setVars($params);
        $view->setVar('translate', $this->services->get('translator'));

        $view->start();
        $view->render('mails', 'styleAdviceActivated');
        $view->finish();

        $subject = $this->translator->_('mails.sendStyleAdviceActivated.subject');
        $body = $view->getContent();

        $this->send($advisor['email'], $subject, $body);
    }

    /**
     * Send style advice reply mail to user
     * 
     * @param array $params
     * @throws MailsException
     */
    public function sendStyleAdviceReplyToUser($params)
    {
        $view = clone $this->mailsView;
        $view->setVars($params);
        $view->setVar('translate', $this->services->get('translator'));

        $view->start();
        $view->render('mails', 'sendStyleAdviceReplyToUser');
        $view->finish();

        $subject = $this->translator->_('mails.sendStyleAdviceReplyToUser.subject');
        $body = $view->getContent();

        $this->send($params['email'], $subject, $body);
    }

    /**
     * Send style advice reply mail to admins
     * 
     * @param array $params
     * @throws MailsException
     */
    public function sendStyleAdviceReplyToAdmin($params)
    {
        $to = $this->services->get('app_config')->application->mails->adminEmail;

        $advisor = $this->services->get('Model_Users')->getUserById($params['style_advisor_id'], 'username');
        $params['advisor'] = $advisor['username'];

        $view = clone $this->mailsView;
        $view->setVars($params);
        $view->setVar('translate', $this->services->get('translator'));

        $view->start();
        $view->render('mails', 'styleAdviceAdmin');
        $view->finish();

        $subject = $this->translator->_('mails.sendStyleAdviceReply.subject');
        $body = $view->getContent();

        $this->send($to, $subject, $body);
    }

    /**
     * Send style advice reply mail to admins
     *
     * @param array $params
     * @throws MailsException
     */
    public function sendStyleAdviceInitiatedToAdmin($params)
    {
        $to = $this->services->get('app_config')->application->mails->adminEmail;
        if (isset($params['style_advisor_id'])) {
            $user = $this->services->get('Model_Users')->getUserById($params['style_advisor_id'], array('username'));
            if ($user) {
                $params['adviser'] = $user['username'];
            }
        }
        $view = clone $this->mailsView;
        $view->setVars($params);
        $view->setVar('translate', $this->services->get('translator'));

        $view->start();
        $view->render('mails', 'styleAdviceInitiatedAdmin');
        $view->finish();

        $subject = $this->translator->_('mails.sendStyleAdviceInitiatedToAdmin.subject');
        $body = $view->getContent();

        $this->send($to, $subject, $body);
    }
}
