<?php

namespace App\Proxies\Frontend;

use App\Initializer\BaseProxy;

class StyleAdvice extends BaseProxy
{
    /**
     * Get all active style advisors (with user_role_id = FASHION_ADVISOR_ROLE_ID)
     * 
     * @return array
     */
    public function getActiveFashionAdvisors()
    {
        $advisors = $this->model->getActiveFashionAdvisors();

        if (!empty($advisors) && is_array($advisors)) {
            $first = true;
            foreach ($advisors as $_k => $_val) {
                if ($first) {
                    $first = false;
                    $advisors[$_k]['checked'] = true;
                } else {
                    $advisors[$_k]['checked'] = false;
                }
            }
            return $this->response($advisors);
        }

        return $this->response('internalError', false);
    }

    /**
     * Save new style advice to db
     * 
     * @param array $params
     * @return array
     */
    public function requestFashionAdvice($params = array())
    {
        $columns = array(
            'user_id', 
            'name', 
            'question', 
            'email', 
            'style_advisor_id'
        );

        $loggedUser = $this->services->get('Proxy_Internal_Account')->getLoggedUser();
        if (!empty($loggedUser)) {
            $params['user_id'] = $loggedUser['id'];
            $params['name'] = $loggedUser['username'];
            $params['email'] = $loggedUser['email'];
        }

        $valid = $this->validator->validateRequestAdviceParams($params);
        if (is_array($valid)) {
            return $this->response($valid, false);
        }

        if ($this->model->insertStyleAdvice($params, $columns)) {
            $this->eventsManager->fire('styleadvice:afterRequestFashionAdvice', $this, $params);
            return $this->response();
        }

        return $this->response(null, false);
    }

}
