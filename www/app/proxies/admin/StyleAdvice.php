<?php
namespace App\Proxies\Admin;
use App\Initializer\BaseProxy;
use App\Models\Tables\StyleAdvice as StyleAdviceTable;
use App\Models\Tables\Users as UsersTable;

class StyleAdvice extends BaseProxy
{
    /**
     * Delete style advice from db
     * 
     * @param array $row
     * @return object
     */
    public function deleteStyleAdvice($row)
    {
        $data = array(
            'id'     => (int) $row['id'],
            'status' => STATUS_DELETED
        );
        $response = $this->model->updateStyleAdvice($data);
        return $this->response($response);
    }

    /**
     * Get all style advice entries from db
     *
     * @param array $filters
     * @return mixed
     */
    public function getStyleAdviceFiltered($filters)
    {
        if (!$this->services->get('Proxy_Internal_Account')->isAdmin()) {
            $filters['style_advisor_id'] = (int) $this->services->get('Proxy_Internal_Account')->getUserId();
            $acceptedStatus = array(STATUS_ACTIVE, STATUS_ANSWERED, STATUS_VALIDATED);
            if (!isset($filters['status']) || !in_array($filters['status'], $acceptedStatus)) {
                $filters['status'] =  $acceptedStatus;
            }
        }

        $response = array(
            'items' => $this->model->getStyleAdviceFiltered($filters),
            'total' => $this->model->getStyleAdviceFiltered($filters, true)
        );

        return $this->response($response);
    }

    /**
     * Update style advice data
     * 
     * @param array $params
     * @return object
     */
    public function updateStyleAdvice($params = array())
    {
        if (!$this->services->get('Proxy_Internal_Account')->isAdmin()) {
            // Accepted statuses from style advisor
            $acceptedStatus = array(STATUS_ACTIVE, STATUS_ANSWERED);
            if (isset($params['status']) && !in_array($params['status'], $acceptedStatus)) {
                return $this->response(array('status' => 'invalidValue'), false);
            }
        }

        $old = $this->model->updateStyleAdvice($params, array('response', 'status', 'observations'));

        if ($old) {
            $this->eventsManager->fire('styleadvice:afterUpdateStyleAdvice', $this, array('old' => $old, 'new' => $params));
            return $this->response();
        }

        return $this->response(null, false);
    }
}
