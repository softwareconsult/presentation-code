var App = angular.module('App', ['mm.foundation']); // 'ui.bootstrap', 'ngTable'
var documentLoaded = false;
$(document).ready(function () {
    var ua = navigator.userAgent;
    if (ua.indexOf('MSIE') > 0) {
        document.body.className += ' ie';
    }
    documentLoaded = true;

    setTimeout(function () {
        $("#search-list-container").css({visibility: "visible"});
    }, 100);

    //nav-fixed
    var width = $(window).width();
    if (width <= 900) {
        $("#nav-fixed #nav > li").click(function (ev) {
            var el = $(this);
            var has = el.has('ul');
            if (has && has[0]) {
                el.addClass('active');
                $("nav.top-bar").addClass('expanded');

                if (ev.currentTarget == ev.target || $(ev.target)[0] == $(ev.currentTarget).find('> a')[0])
                    return false;
            }
        });
    }

    $("body").removeClass("jsDisabled").addClass("jsEnabled");
});

// When you hit back button from browser and the url was pushed with history.pushState it won't refresh the page but it will trigger onpopstate
window.onpopstate = function (event) {
    //console.log('>>>>>>>>POP STATE: ');
    var ua = navigator.userAgent;
    if (ua.indexOf('Safari') > 0 && ua.indexOf('Chrome') === -1) {
        console.log('Safari');
    } else {
        window.location = window.location.href;
    }
};

/**
 * Body wrapper controller
 */
App.controller('applicationWrapperController', [
    "$scope", "$rootScope", "$modal", "$location", "apiService", "Newsletter", "$sce",
    function ($scope, $rootScope, $modal, $location, apiService, Newsletter, $sce) {
        $scope.init = function() {
            // initialize the popup
            Newsletter.initPopup();
        };

        // Initialize route assembler
        Url.init(__routes, Config.baseUri);

        $scope.language = Language;

		$scope.constants = Constants;
        $scope.translate = translate;

        $scope.Currencies = Currencies;

        $rootScope.userSession = App.lu;
        $scope.url = Url;

        // Get notification number
        $scope.notificationNumber = 0;
        $rootScope.$watch('userSession.id', function () {
            // If the user id is a numeric value
            if (
                typeof $rootScope.userSession === 'object' &&
                $rootScope.userSession !== null &&
                /^\d+$/.test($rootScope.userSession.id)
            ) {
                apiService.call('Notifications.getLoggedUserNotificationNumber',{'region_code': Region.selected}, function (response) {
                    if (response && response.success) {
                        $scope.notificationNumber = parseInt(response.data);
                        digest($scope);
                    }
                });
            }
        });

        $scope.api_url = Api.url;

        $rootScope.$on('avatarChanged', function (e, data) {
            $rootScope.userSession.avatar = data;
            digest($rootScope);
        });

        $scope.newsletterSubscribe = function (email, place) {
			if (typeof this.useModal != 'undefined') {
				$scope.useModal = true;
			} else {
				$scope.useModal = false;
			}
            Newsletter.subscribe(email, place, function(response) {
				var modalHtml = '<div>' +
									'<a class="close-reveal-modal" data-ng-click="closeModal()">&#215;</a>' +
									'<div class="columns block row newsletter-text">' +
										'<span>%message%</span>' +
										'<a class="close-reveal-modal-button" data-ng-click="closeModal()">' + translate('ok') + '</a>' +
									'</div>' +
								'</div>';
                if (response && response.success) {
					modalHtml = modalHtml.replace('%message%', translate('newsletter.success_subscribe'));
//					var modalHtml = '<div>' +
//										'<a class="close-reveal-modal" data-ng-click="closeModal()">&#215;</a>' +
//										'<div class="columns block row newsletter-text">' +
//											'<span>' + translate('newsletter.success_subscribe') +'</span>' +
//											'<a class="close-reveal-modal-button" data-ng-click="closeModal()">' + translate('ok') + '</a>' +
//										'</div>' +
//									'</div>';
                } else {
					modalHtml = modalHtml.replace('%message%', translate(response.data.email));
//					var modalHtml = '<div>' +
//										'<a class="close-reveal-modal" data-ng-click="closeModal()">&#215;</a>' +
//										'<div class="columns block row newsletter-text">' +
//											'<span>' + translate(response.data.email) +'</span>' +
//											'<a class="close-reveal-modal-button" data-ng-click="closeModal()">' + translate('ok') + '</a>' +
//										'</div>' +
//									'</div>';
                }
				var modalInstance = $modal.open({
                    template: modalHtml,
                    windowClass: 'tiny newsletter-response-popup',
                    backdrop: 'static',
                    controller: 'newsletterPopupController'
                });
                digest($scope);
            });
        };

        $scope.changeCurrency = function(currency) {
            apiService.callLocal('/common/changeCurrency', {'currency': currency}, function(response) {
                if (response && response.success) {
                    window.location.reload();
                } else {
                    popupNotification.show('currency', 'currencyChangeError');
                }
            });
        };

        $scope.changeLanguage = function(language) {
            if (typeof language === 'string') {
                language = JSON.parse(language);
            }

            if (typeof language !== 'object' || typeof language.code === 'undefined'|| language.code == Language.selected) {
                return;
            }

            if (Config.app_env !== 'production') {
                apiService.callLocal('/common/changeLanguage', {'language': language}, function(response) {
                    if (response && response.success) {
                        window.location.href = Url.get({'for': Constants.URL_HOMEPAGE});
                    } else {
                        alert("Something is wrong with the languages");
                    }
                });

                return;
            }

            if (Config.app_env === 'production') {
                var curDom = window.location.origin.split('.');
                curDom[curDom.length -1] = (language.code == 'en' ? 'com' : language);
                window.location.href = curDom.join('.');
            }

        };

        $scope.processPriceForView = function(price, htmlSymbol) {
            return $sce.trustAsHtml(processPriceForView(price, htmlSymbol));
        };

        $scope.Currency = Currency;

        $scope.requestFashionAdvice = function() {
            var modalInstance = $modal.open({
                templateUrl: 'fashionAdvice',
                scope: $scope,
                controller: 'modalFashionAdviceController',
                windowClass: 'fashion-advice-modal tiny'
            });
        }
    }]);

/**
 * Used in modals where is only needed to close the modal
 */
App.controller('modalCloseController', ["$scope", "$modalInstance", function ($scope, $modalInstance) {
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.closeModal = function () {
        $modalInstance.dismiss('cancel');
    };
    // Alias of close
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

App.filter('processPriceForView', function() {
    return function (value, htmlSymbol) {
        return processPriceForView(value, htmlSymbol)
    };
});

var digest = function (scope) {
    // Digest the modification
    if (!scope.$$phase) {
        scope.$digest();
    }
};

var translate = function (key, placeholders, strict) {
    if (typeof strict !== 'boolean') {
        strict = true;
    }

    if (typeof __translations === 'object' && typeof __translations[key] !== 'undefined') {
        if (typeof placeholders !== 'object') {
            return __translations[key];
        }

        var tempText = __translations[key];
        $.each(placeholders, function (index, value) {
            tempText = tempText.replace("%" + index + "%", value);
        });

        tempText = tempText.replace(/(%([a-zA-Z_]+)%)/gi, '');
        return tempText;
    }

    if (typeof (Config.app_env) !== 'undefined' && Config.app_env !== 'production' && Config.app_env !== 'staging' && strict) {
        //return key;
        alert('There is no translation for key: ' + key);
    } else {
        return key;
    }
};

var processPriceForView = function(price, htmlSymbol) {
    price = Math.round(price * 100) / 100;

    if (typeof htmlSymbol === 'boolean' && !htmlSymbol) {
        return price + ' ' + Currency.symbols[Currency.selected];
    }

    return price + '  <span property="priceCurrency" itemprop="currency" class="currency" content="' + Currency.selected + '">' + Currency.htmlSymbols[Currency.selected] + '</span>';
};