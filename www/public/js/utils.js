/**
 * Created by software-consult.ro on 7/10/14.
 */

/**
 *
 * @param {type} $locationProvider
 * @returns {undefined}
 */
function locationProviderSetConfig(locationProvider) {
    var ua = navigator.userAgent;
    if (ua.indexOf('MSIE 9') > 0) {
        locationProvider.html5Mode(false).hashPrefix("!");
        var hash = location.hash;
        if (hash.indexOf('#!/') > -1) {
            window.location.href = hash.replace(/#!/g, '');
            window.location.href.substr(0, window.location.href.indexOf('#!'));
        }
    } else {
        locationProvider.html5Mode(true).hashPrefix('!');

    }
    //this is used to not affcet the page change action 
    $("a").filter(":not([target])").attr('target', '_self');
}


/**
 * Copies the behaviour of strpos from PHP
 *
 * @param needle
 * @param offset
 * @return string
 */
String.prototype.strpos = function (needle, offset) {
    var i = (this + '').indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
};

/**
 * Copies the behaviour of ucfirst from PHP
 *
 * @param str
 * @returns {string}
 */
function ucfirst(str) {
    str += '';
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1);
}

/**
 * Clear empty values from an object, recursively
 *
 * @param list
 * @returns {*}
 */
var clearObjectEmptyValues = function (list) {
    var tmp = list;

    if (list && typeof (list) === 'object') {
        $.each(list, function (key, value) {

            if (value === '' || value === null) {
                delete tmp[key];
            }

            if (value && typeof (value) === 'object') {
                if (value.length == 0 || value[0] === '') {
                    delete tmp[key];
                } else {
                    tmp[key] = clearObjectEmptyValues(value);
                }
            }
        });
    }

    return tmp;
};

/**
 *
 * @param dayScope
 * @param montScope
 * @param yearScope
 * @returns {String}
 */
function joinDate(dayScope, montScope, yearScope) {
    var date = null;
    if (typeof dayScope != "undefined" && typeof montScope != "undefined"
        && typeof yearScope != "undefined") {
        if (parseInt(dayScope) != NaN && parseInt(montScope) != NaN
            && parseInt(yearScope) != NaN) {
            date = dayScope + "-" + montScope + "-" + yearScope;
        }
    }
    return date;
}
/**
 *
 * @param dayScope
 * @param montScope
 * @param yearScope
 * @TODO - check day
 * @todo - add month name and translations
 * @todo - check year in the future
 */
function dateDropdownValues(dayScope, montScope, yearScope) {
    for (var d = 1; d <= 31; d++) {
        dayScope.push({
            "id": d,
            "name": d
        });
    }
    for (var m = 1; m <= 12; m++) {
        montScope.push({
            "id": m,
            "name": m
        });
    }
    for (var y = new Date().getFullYear() - 14; y >= 1940; y--) {
        yearScope.push({
            "id": y,
            "name": y
        });
    }
}

function getRequestUri() {
    return window.top.location.pathname + window.top.location.search + window.top.location.hash;
}

function isLogged() {
    if (typeof App !== 'undefined' && typeof App.lu !== 'undefined' && App.lu !== null) {
        return true;
    }

    return false;
}

function getLoggedUserId() {
    if (isLogged()) {
        return App.lu.id;
    }

    return 0;
}

function isLiked(item_type, item_id) {
    if (
        isLogged() &&
        typeof App.lu.liked_items !== 'undefined' &&
        App.lu.liked_items !== null &&
        typeof App.lu.liked_items[parseInt(item_type)] !== 'undefined' &&
        typeof App.lu.liked_items[parseInt(item_type)]['' + item_id] !== 'undefined'
    ) {
        return true;
    }

    return false;
}

/**
 *
 * @param obj data
 * @param obj list
 * @returns {*|{}}
 */
var setColumnAsKey = function (data, columnName, list) {
    var sorted = list || {};
    if (typeof data === 'object' && data != null) {
        $.each(data, function (key, val) {
            sorted[val[columnName]] = val;
            if (typeof val.children != "undefined") {
                setColumnAsKey(val.children, columnName, sorted);
            }
        });
    }
    return sorted;
};

function number_format(number, decimals, dec_point, thousands_sep) {
    //  discuss at: http://phpjs.org/functions/number_format/
    // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: davook
    // improved by: Brett Zamir (http://brett-zamir.me)
    // improved by: Brett Zamir (http://brett-zamir.me)
    // improved by: Theriault
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: Michael White (http://getsprink.com)
    // bugfixed by: Benjamin Lupton
    // bugfixed by: Allan Jensen (http://www.winternet.no)
    // bugfixed by: Howard Yeend
    // bugfixed by: Diogo Resende
    // bugfixed by: Rival
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    //  revised by: Luke Smith (http://lucassmith.name)
    //    input by: Kheang Hok Chin (http://www.distantia.ca/)
    //    input by: Jay Klehr
    //    input by: Amir Habibi (http://www.residence-mixte.com/)
    //    input by: Amirouche
    //   example 1: number_format(1234.56);
    //   returns 1: '1,235'
    //   example 2: number_format(1234.56, 2, ',', ' ');
    //   returns 2: '1 234,56'
    //   example 3: number_format(1234.5678, 2, '.', '');
    //   returns 3: '1234.57'
    //   example 4: number_format(67, 2, ',', '.');
    //   returns 4: '67,00'
    //   example 5: number_format(1000);
    //   returns 5: '1,000'
    //   example 6: number_format(67.311, 2);
    //   returns 6: '67.31'
    //   example 7: number_format(1000.55, 1);
    //   returns 7: '1,000.6'
    //   example 8: number_format(67000, 5, ',', '.');
    //   returns 8: '67.000,00000'
    //   example 9: number_format(0.9, 0);
    //   returns 9: '1'
    //  example 10: number_format('1.20', 2);
    //  returns 10: '1.20'
    //  example 11: number_format('1.20', 4);
    //  returns 11: '1.2000'
    //  example 12: number_format('1.2000', 3);
    //  returns 12: '1.200'
    //  example 13: number_format('1 000,50', 2, '.', ' ');
    //  returns 13: '100 050.00'
    //  example 14: number_format(1e-8, 8, '.', '');
    //  returns 14: '0.00000001'

    number = (number + '')
        .replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + (Math.round(n * k) / k)
                    .toFixed(prec);
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '')
            .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
            .join('0');
    }
    return s.join(dec);
}
