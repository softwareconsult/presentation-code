/**
 * create custom dropDown, need angular.foundation.js
 */
App.directive('dropdown', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            ngClick: "&ngClick",
            model: "=model",
            list: "=list",
            value: "=value"
        },
        link: function (scope, ele, attrs) {
            scope.text = attrs.text;
            scope.id = attrs.id;
            scope.filterd = true;
            scope.showIt = false;
            scope.showFilter = false;
			scope.translate = translate;
            var changeDefaultValue = function (model, list) {
                if (typeof model !== 'undefined'
                    && model !== null && model !== '') {
                    if (typeof list !== 'undefined'
                        && list !== null && list.length > 0) {
                        $.each(list, function (index, val) {
                            if (val.id == model) {
                                scope.input_model = translate(val.name, false, false);
                                scope.model = val.id;
                                return;
                            }
                        });
                    }
                }
            };

            scope.$watch('model', function () {
                changeDefaultValue(scope.model, scope.list);
            });
            scope.$watch('list', function () {
                changeDefaultValue(scope.model, scope.list);
            });
            scope.filterList = function (option) {
                scope.showFilter = true;
                scope.showIt = false;
            };
            scope.change = function (option) {
                scope.input_model = translate(option.name, false, false);
                scope.model = option.id;
            };

            scope.displayIt = function (value) {
                scope.showIt = !value;
                scope.showFilter = false;
            };

            var resetElemWidth = function() {
                // In popup it needs to be executed last so it will take the size accordingly
                setTimeout(function() {
                    scope.elemWidth = ele[0].offsetWidth - 2; // 2 pixels is the border from left and right
                }, 0);
            };

            resetElemWidth();

            scope.$on('dropdown.reset', function() {
                resetElemWidth();
            });

        },
        template: '<div class="dropdown-wrapper" id="dropdown-wrapper-{{id}}" data-ng-click="displayIt(showIt)" data-ng-mouseleave="displayIt(true)">' +
        '<input ng-model="input_model" data-ng-change="filterList(input_model)" placeholder="{{text}}" style="z-index:999" />' +
        '<ul id="dropdown_{{id}}" class="f-dropdown dropdown-menu no-margin" data-ng-show="showIt||showFilter" style="min-width: {{elemWidth}}px;">' +
        '<li data-ng-show="showFilter" data-ng-cloak data-ng-repeat="option in list | filter:input_model" data-ng-click="change(option)">{{translate(option.name, false, false)}}</li>' +
        '<li data-ng-show="showIt" data-ng-cloak data-ng-repeat="option in list" data-ng-click="change(option)">{{translate(option.name, false, false)}}</li>' +
        '</ul>' +
        '</div>'
    };
});

/**
 * load  image
 */
App.directive('avatar', function () {
    return {
        restrict: 'EA',
        transclude: true,
        replace: true,
        scope: {
            image: "=image"
        },
        link: function (scope, ele, attrs) {
            scope.class = attrs.class;
            scope.imgurl = Config.img.url;
            scope.size = attrs.imageSize;
            if (typeof scope.size == 'undefined') {
                scope.size = 107;
            }

            ele.bind('error', function () {
                $(ele).attr('src', '/img/1.png');
            });
        },
        template: '<img alt="" ng-src="{{imgurl}}/avatars/{{size}}/{{image}}" class="{{class}}">'
    };
});
App.directive('commentAvatar', function () {
    return {
        restrict: 'C',
        replace: true,
        scope: {},
        link: function (scope, ele, attrs) {
            scope.$watch("gender", function () {
                if (typeof scope.gender !== undefined) {
                    ele.bind('error', function () {
                        $(ele).attr('src', '/img/1.png');
                    });
                }
            });
        }
    };
});

/**
 * submit on enter
 */
App.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});

/**
 * used if need to call something when ng-repeat end $compile all elements
 */
App.directive('onLastRepeat', function () {
    return function (scope, element, attrs) {
        if (scope.$last)
            setTimeout(function () {
                scope.$emit('onRepeatLast', element, attrs);
            }, 1);
    };
});
/**
 * need to be put on all product Images
 */
App.directive('onImageError', function () {
    return function (scope, element, attrs) {
        $(element[0]).error(function () {
            if (attrs.onImageError == 'hide') {
                $(this).hide();
            } else {
                $(this).attr('src', '/img/image-not-available.png');
            }
        });
    };
});
/**
 * used for top navigator
 */
App.directive('topNavActions', function () {
    return function (scope, element, attrs)
    {
        var width = $(window).width();
        if (width > 900) {
            var current = element.find('.active');
            element.find(' > li').on('mouseenter', function () {
                current.removeClass('active');
            }).on('mouseleave', function () {
                current.addClass('active');
            });
        }
    };
});
/**
 * used for top navigator
 */
App.directive('stickyNavigator', ["$window", function ($window) {
    return {
        restrict: 'AC',
        scope: {},
        link: function (scope, element, attrs) {
            var $w = angular.element($window);
            var navHomeY = element.offset().top;
            var isFixed = false;

            var isMobileView,
                shouldBeFixed,
                scrollTop;

            $w.scroll(function () {
                isMobileView = $w.width() <= 900;
                shouldBeFixed = ($w.scrollTop() > navHomeY) && ($w.scrollTop() > 0);
                if (shouldBeFixed && !isFixed && !isMobileView) {
                    scrollTop = $w.scrollTop();
                    element.css({
                        position: 'fixed',
                        top: 0,
                        zIndex: 100,
                        left: element.offset().left,
                        width: "100%"
                    });
                    isFixed = true;
                    element.parent().height(navHomeY + element.outerHeight());
                    $w.scrollTop(scrollTop);
                } else if (!shouldBeFixed && isFixed) {
                    element.css({
                        position: 'static'
                    });

                    element.parent().height('auto');
                    isFixed = false;
                }
            });
        }
    };
}]);

/**
 * used for footer
 */
App.directive('stickyFooter', [function () {
    return {
        restrict: 'AC',
        link: link
    };

    function link(scope, element, attrs) {
        // Element selectors
        var content = $('.PageWrapper');
        var windowObj = $(window);
        var positionFooter = function() {
            var contentHeight = content.outerHeight();
            var cssProperties = { position: 'relative' };
            if(contentHeight < $(window).height()) {
                cssProperties = {
                    position: 'fixed',
                    bottom: 0
                };
            }
            $(element).css(cssProperties);
        };
        windowObj.on('resize', positionFooter);
        content.on('resizedHeight', positionFooter);
        positionFooter();
    };
}]);

/**
 * triggers resizedHeight event on the current element if the content of the page changes
 */
App.directive('PageWrapper', [function () {
    return {
        restrict: 'AC',
        link: link
    };

    function link(scope, element, attrs) {
        scope.$watch(
            function() {
                return element.height();
            },
            function(height) {
                $(element).trigger('resizedHeight', [height]);
            });
    };
}]);

App.directive('showInfo', function () {
    return {
        restrict: 'AC',
        scope: {
            'show-info': '='
        },
        link: function (scope, element, attrs) {
            var DIV = $("<div class='custom-modal-tooltip'></div>");
            element.on('mouseenter', function () {
                var position = {};
                var offset = element.offset();
                var tooltipText = attrs.showInfo;
                if (offset.left > 700) {
                    position = {
                        "left": "-75px",
                        "top": "50px"
                    };
                } else {
                    position = {
                        "left": "75px",
                        "top": "50px"
                    };
                }
                DIV.css(position).text(tooltipText);
                if (!$(element).next().hasClass('custom-modal-tooltip') && tooltipText) {
                    $(element).after(DIV);
                } else {
                    DIV.show();
                }
            }).on('mouseleave', function () {
                DIV.hide();
            });
            DIV.on('mouseenter', function () {
                $(this).show();
            }).on('mouseleave', function () {
                $(this).hide();
            });
            DIV.on('click', function () {
                $(element).trigger('click');
            })
        }
    };
});

App.directive('clickOutside', ["$window", "$parse", function ($window, $parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var clickOutHandler = $parse(attrs.clickOutside);

            $(window).on('click', function (event) {

                if (element[0].contains(event.target)) {
                    return;
                }

                clickOutHandler(scope, {$event: event});
                scope.$apply();
            });
        }
    };
}]);
App.directive('colorPicker', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            ngClick: "&ngClick",
            model: "=model",
            list: "=list",
            value: "=value"
        },
        link: function (scope, element, attrs) {
            scope.text = attrs.text;
            scope.selected = false;
            scope.id = attrs.id;
			scope.translate = translate;
            var changeDefaultValue = function (model, list) {
                if (typeof model !== 'undefined'
                    && model !== null && model !== '') {
                    if (typeof list !== 'undefined'
                        && list !== null && list.length > 0) {
                        $.each(list, function (index, val) {
                            if (val.id == model) {
                                scope.model = val.id;
                                return;
                            }
                        });
                    }
                }
            };

            scope.$watch('model', function () {
                changeDefaultValue(scope.model, scope.list);
            });
            scope.$watch('list', function () {
                changeDefaultValue(scope.model, scope.list);
            });
            scope.change = function (option) {
                if (option === false || scope.model === option.id) {
                    scope.selected = false;
                    scope.model = null;
                } else {
                    scope.selected = option.code;
                    scope.model = option.id;
                }
                scope.showIt = false;
            };

            var eventRegistered = false;
            scope.toggleColorsList = function() {
                if (!eventRegistered) {
                    $(window).on('click', function (event) {
                        if (element[0].contains(event.target)) {
                            return;
                        }
                        scope.showIt = false;
                    });
                }

                scope.showIt = !scope.showIt;
            }

        }, //color-picker-dropdown
        template: '<div class="color-selector-wrapper">' +
                    '<a id="color-selector-dropdown" href="" data-ng-click="toggleColorsList()">' +
                        '<span data-ng-show="selected" class="color">' +
                            '<span data-ng-style="{\'background-color\' : selected}">&nbsp;</span>' +
                        '</span>' +
                        '<span data-ng-show="!selected">{{text}}</span>' +
                    '</a>' +
                    '<div id="color-picker-list" class="f-dropdown" data-ng-show="showIt">' +
                        '<span class="all" data-ng-click="change(false)" title="">' + translate('color.picker.allcolors') + '</span>' +
                        '<span data-ng-repeat="option in list" class="color">' +
                            '<span data-ng-click="change(option)" data-ng-style="{\'background-color\' : option.code}" title="{{translate(option.name, false, false)}}">&nbsp;</span>' +
                        '</span>' +
                    '</div>' +
                    '<div class="clear-fix"></div>' +

                '</div>'
    };
});

App.directive('checkbox', function () {
    return {
        restrict: 'E',
        scope: {
            value: '@?',
            checked: '=?',
            checkedValues: '=?',
            'class': '=?',
            'label': '@?'
        },
        link: function (scope, elem, attrs) {
            // Set the default is not exists
            if (typeof scope.checked === 'undefined' || scope.checked === '') {
                scope.checked = false;
            }

            // Make the conversion from string or from int
            if (typeof scope.checked !== 'undefined' && (scope.checked == 0 || scope.checked == '0')) {
                scope.checked = false;
            }

            if (typeof scope.checked !== 'undefined' && (scope.checked == 1 || scope.checked == '1')) {
                scope.checked = true;
            }

            scope.$watch('checkedValues', function() {
                if (typeof scope.checkedValues === 'object') {
                    scope.checked = (scope.checkedValues.indexOf(scope.value) !== -1);
                }
            }, true);

            scope.stateChange = function () {
                scope.checked = !scope.checked;

                if (typeof scope.checkedValues !== 'object') {
                    scope.checkedValues = [];
                }
                var indexOf = scope.checkedValues.indexOf(scope.value);
                if (scope.checked && indexOf === -1) {
                    scope.checkedValues.push(scope.value);
                }
                else if (!scope.checked && indexOf !== -1) {
                    scope.checkedValues.splice(indexOf, 1);
                }
            };

            if (typeof scope.value === 'undefined') {
                scope.value = null;
            }


        },
        template: '<div class="custom-ckeckbox-wrapper {{class}}" data-ng-click="stateChange()">' +
        '<div class="is-checked-block" data-ng-show="checked" ></div>' +
        '</div>' +
        '<label data-ng-cloak data-ng-click="stateChange()">{{label}}</label>'
    };
});

App.directive('radio', function () {
    return {
        restrict: 'E',
        scope: {
            value: '@',
            checked: '=',
            'label': '@?'
        },
        template: '<div class="custom-radio-wrapper" data-ng-click="checked = value">' +
                    '<div class="is-checked-block" data-ng-show="checked == value" ></div>' +
                '</div>' +
                '<label data-ng-cloak data-ng-click="checked = value">{{label}}</label>'


    };
});
App.directive('onNgRepeatComplete', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            if (scope.$last) {
                scope.$eval(attrs.onNgRepeatComplete);
            }
        }
    };
});

App.directive('banner', ["apiService", function (apiService) {
    var cookieName = '',
        cookieSetValue = "yes",
        cookieSetExpire = 1;
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs) {
			var cookieName = 'cookie-' + $(element).attr('id') + '-' + Language.selected + '-hide';
			var hide = Cookies.get(cookieName);
			if (hide !== 'yes') {
				if (typeof attrs.banner !== 'undefined') {
					apiService.call('Banners.getBannersByZone', {
						'zone': attrs.banner,
						'language_code': Language.selected
					}, function (response) {
						if (response.success) {
							scope.url = Config.img.url;
							scope.zone = attrs.banner;
							scope.data = response.data;
							scope.nofollow = (response.data.nofollow == 1) ? 'nofollow' : '';
							scope.blank = (response.data.blank == 1) ? '_blank' : '_self';
							if (attrs.closeBanner) {
								scope.containerID = attrs.id;
								scope.coockieEpirationDays = parseInt(attrs.closeBanner);
								$(element).find('.close-button').show();
							} else {
								$(element).find('.close-button').hide();
							}
							$(element).show();
						} else {
							$(element).hide();
						}
					});
				}
			}
        },
		controller: ["$scope", function($scope){
			$scope.setCookie = function($event) {
				$event.preventDefault();
				cookieSetExpire = $scope.coockieEpirationDays;
				cookieName = 'cookie-' + $scope.containerID + '-' + Language.selected + '-hide';
				Cookies.set(cookieName, cookieSetValue, {expires: cookieSetExpire, path: '/'});
				$('#' + $scope.containerID).hide();
			};
		}],
        template: '<a href="{{data.url}}" rel="{{nofollow}}" target="{{blank}}">' +
                    '<img src="{{url}}/banners/{{zone}}/{{data.banner}}" width="" alt="" class="banner-image" />' +
					'<span class="close-button" ng-click="setCookie($event)">x</span>' + 
                '</a> '
    };
}]);

App.directive('removeNoFollow', function(){
    return {
        scope: {},
        link: function(scope, element, attributes){
            element.removeAttr('rel');
        }
    }
});

App.directive('itemDescription', ["$sce", function ($sce) {
    return {
        restrict: 'A',
        scope: {},
        compile: function () {
            return {
                pre: function (scope, element, attributes) {
                    attributes.$observe('itemDescription', function (val) {
                        function url2link(str) {
                            var match = str.match(/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/gi);
                            if (match != null && match.length > 0) {
                                $.each(match, function (k, val) {
                                    var link = "<a target='_blank' rel='nofollow' href='" + val + "'>" + val + "</a>"
                                    str = str.replace(val, link);
                                });
                            }
                            return str;
                        }

                        if (typeof val !== 'undefined') {
                            scope.description = $sce.trustAsHtml(url2link(val));
                        }
                    });
                },
                post: function (scope, element, attributes) {
                    scope.descriptionHeight = 40;
                    scope.$watch('description', function (val) {
                        if (typeof val !== 'undefined') {
                            scope.descriptionText = $(element).find('.item-description');
                            scope.descriptionOriginalHeight = scope.descriptionText.height();
                            if (scope.descriptionOriginalHeight > scope.descriptionHeight) {
                                scope.descriptionText.height(scope.descriptionHeight);
                                $(element).find('.more-details-wrapper').show();
                            } else {
                                $(element).find('.more-details-wrapper').hide();
                            }

                        }
                    });

                    /**
                     * used for more|less product description
                     * @param event e
                     * @returns void
                     */
                    $(element).find(".description-more-details:first").on('click', function (e) {
                        var obj = $(scope.descriptionText[0]), self = $(this);
                        if (obj.height() > scope.descriptionHeight) {
                            obj.height(scope.descriptionHeight);
                            self.text(translate('products.more_details'));
                        } else {
                            $(obj).height(scope.descriptionOriginalHeight);
                            self.text(translate('products.less_details'));
                        }
                    });

                }
            }
        },
        template: ' <span itemprop="description" class="item-description display-inline-block" data-ng-bind-html="description"></span>' +
        '<hr/> <div class="more-details-wrapper"><a href="" class="description-more-details">' +
        translate('products.more_details') +
        '</a><hr/></div>'
    };
}]);

/**
 * Duplicated above function as it allowed for
 * bad html to be parsed directly.
 */
App.directive('untrustedItemDescription', function () {
    return {
        restrict: 'A',
        scope: {},
        compile: function () {
            return {
                pre: function (scope, element, attributes) {
                    attributes.$observe('untrustedItemDescription', function (val) {
                        function url2link(str) {
                            var match = str.match(/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/gi);
                            if (match != null && match.length > 0) {
                                $.each(match, function (k, val) {
                                    var link = "<a target='_blank' rel='nofollow' href='" + val + "'>" + val + "</a>"
                                    str = str.replace(val, link);
                                });
                            }
                            return str;
                        }

                        if (typeof val !== 'undefined') {
                            scope.description = val;
                        }
                    });
                },
                post: function (scope, element, attributes) {
                    scope.descriptionHeight = 40;
                    scope.$watch('description', function (val) {
                        if (typeof val !== 'undefined') {
                            scope.descriptionText = $(element).find('.item-description');
                            scope.descriptionOriginalHeight = scope.descriptionText.height();
                            if (scope.descriptionOriginalHeight > scope.descriptionHeight) {
                                scope.descriptionText.height(scope.descriptionHeight);
                                $(element).find('.more-details-wrapper').show();
                            } else {
                                $(element).find('.more-details-wrapper').hide();
                            }

                        }
                    });

                    /**
                     * used for more|less product description
                     * @param event e
                     * @returns void
                     */
                    $(element).find(".description-more-details:first").on('click', function (e) {
                        var obj = $(scope.descriptionText[0]), self = $(this);
                        if (obj.height() > scope.descriptionHeight) {
                            obj.height(scope.descriptionHeight);
                            self.text(translate('products.more_details'));
                        } else {
                            $(obj).height(scope.descriptionOriginalHeight);
                            self.text(translate('products.less_details'));
                        }
                    });

                }
            }
        },
        template: ' <span itemprop="description" class="item-description display-inline-block" ng-bind="description"></span>' +
        '<hr/> <div class="more-details-wrapper"><a href="" class="description-more-details">' +
        translate('products.more_details') +
        '</a><hr/></div>'
    };
});

App.filter('hasHTTP', function () {
    return function (item) {
        if (typeof item != "undefined" && item != '' && item != null) {
            if (item.indexOf("http://") > -1 || item.indexOf("https://") > -1) {
                return item;
            } else {
                return "http://" + item;
            }
        }
    };
});
App.filter('nospace', function () {
    return function (value) {
        return (!value) ? '' : value.replace(/ /g, '');
    };
});

App.directive('socialShareBar', function () {
    return {
        restrict: 'EA',
		scope: true,
        template:
			'<div class="social-list medium-6 columns small-only-text-center medium-text-left"  id="social-list" >' +
				'<span>' + translate('share') + ':</span>' +
				'<a class="icon30 facebook" target="_blank" href="" ng-href={{facebookShareLink}}></a>' +
				'<a class="icon30 twitter" target="_blank" href="" ng-href={{twitterShareLink}}></a>' +
				'<a class="icon30 gplus" target="_blank" href="" ng-href={{googleShareLink}}></a>' +
				'<a class="icon30 linkedin" target="_blank" href="" ng-href={{linkedinShareLink}}></a>' +
				'<a class="icon30 pinterest" target="_blank" href="" ng-href={{pinterestShareLink}}></a>' +
				'<a class="icon30 tumblr" target="_blank" href="" ng-href={{tumblrShareLink}}></a>' +
				'<a class="icon30 rss" target="_blank" href="{{url.get({\'for\' : constants.URL_BLOG_FEED})}}"></a>' +
			'</div>' +
			'<div class="email-subscription medium-6 columns small-only-text-center medium-text-right">' +
				'<span data-ng-click="newsletterSubscribe(email, \'{{pageLink}}\')">' + translate('email.subscription.label') + '</span>' +
				'<input type="text" data-ng-enter="newsletterSubscribe(email, \'{{pageLink}}\')" placeholder="' + translate('email.subscription.placeholder') + '" data-ng-model="email" />' +
			'</div>' +
			'<div class="clear-fix"></div>',
		controller: ["$scope", function($scope) {
			$scope.$on('$locationChangeSuccess', function() {
				$scope.facebookShareLink = "https://www.facebook.com/sharer.php?u=" + encodeURI(window.location.href) + "&t=" + encodeURI(document.title);
				$scope.twitterShareLink = "https://twitter.com/share?url=" + encodeURI(window.location.href) + "&text=" + encodeURI(document.title) + "&hashtags=software-consult.ro";
				$scope.googleShareLink = "https://plus.google.com/share?url=" + encodeURI(window.location.href);
				$scope.linkedinShareLink = "https://www.linkedin.com/shareArticle?mini=true&url=" + encodeURI(window.location.href) + "&title=" + encodeURI(document.title) + "&summary=" + encodeURI($("meta[name='description']").attr('content')) + "&source=" + encodeURI(translate('software-consult.ro'));
				$scope.pinterestShareLink = "https://www.pinterest.com/pin/create/button/?url=" + encodeURI(window.location.href) + "&description=" + encodeURI(document.title);
				$scope.tumblrShareLink = "http://www.tumblr.com/share/link?url=" + encodeURI(window.location.href);
				$scope.pageLink = encodeURI(window.location.href);
			});
		}]
    };
});

App.directive('appDownload', function () {
	var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    var cookieName = 'cookie-app-link-hide',
        cookieSetValue = "yes",
        cookieSetExpire = 365;
    return {
        restrict: 'C',
		scope: {},
        template:
			'<div class="app-download-wrapper">' +
				'<div class="app-download-container">' +
					'<a href="{{app_link}}" target="_blank" class="{{app_link_class}}" data-ng-click="setCookie()" />' +
					'<a class="button secondary small" title="' + translate('appDownload.hide') + '" data-ng-click="setCookie()">' +
						translate('appDownload.hide') +
					'</a>' +
				'</div>' +
			'</div>',
        link: function (scope, ele, attrs) {
			var hide = Cookies.get(cookieName);
			if (hide !== "yes") {
				if ( userAgent.match(/Android/i) ) {
					$('.app-download').show();
				} else {
					$('.app-download').hide();
				}
//				if ( userAgent.match(/Android/i) || userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i) ) {
//					$('.app-download').show();
//				}
			} else {
				$('.app-download').hide();
			}
        },
		controller: ["$scope", function($scope) {
			function init() {
				$scope.hide = Cookies.get(cookieName);
				if ($scope.hide !== "yes") {
					if ( userAgent.match(/Android/i) ) {
						$scope.app_link = "http://play.google.com/store/apps/details?id=";
						$scope.app_link_class = "google-play";
					}
//					} else if ( userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i) ) {
//						$scope.app_link = "http://appstore.com/";
//						$scope.app_link_class = "app-store";
//					}
				}
			}
			init();

			$scope.setCookie = function() {
				Cookies.set(cookieName, cookieSetValue, {expires: cookieSetExpire, path: '/'});
				$scope.hide = cookieSetValue;
				$('.app-download').hide();
			};
		}]
    };
});