angular.module('tags', [])
    .service('tagsService', function () {
        this.tags = [];

        /**
         * Process the incoming tags (string or array)
         *
         * @param tags
         * @returns {Array}
         */
        this.addIncomingTags = function (tags) {
            var unique = [];
            var tmpTags = angular.copy(tags);
            if (typeof tmpTags !== 'undefined' && tmpTags !== null) {
                if (typeof tags === 'string') {
                    var tmpTags = tags.split(',');
                }
                $.each(tmpTags, function (i, el) {
                    el = el.trim();
                    if (el !== '') {
                        if ($.inArray(el, unique) === -1) {
                            unique.push(el);
                        }
                    }
                });
            }

            this.tags = unique;
        };

        this.addTag = function (tag) {
            var self = this;
            tag = tag.trim();
            tag = tag.split(',');
            $.each(tag, function (index, value) {
                value = value.trim();
                if (value !== '' && $.inArray(value, self.tags) === -1) {
                    self.tags.push(value);
                }
            });
        };

        this.deleteTag = function (index) {
            this.tags.splice(index, 1);
        };

        this.getOutgoingTags = function () {
            return this.tags;
        };
    })
    .directive('autocompleteTags', ["apiService", "tagsService", function (apiService, tagsService) {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                tags: '='
            },
            link: function (scope, ele, attrs) {
                var charLimit = 3;
                scope.recommendedTagsList = [];
                scope.placeholder = attrs.placeholder;
                scope.id = attrs.id;
                scope.autocompleteValue = '';
                scope.toShort = false;
                scope.elemWidth = ele[0].offsetWidth - 2; // 2 pixels is the border from left and right

                scope.$watch('tags', function () {
                    tagsService.addIncomingTags(scope.tags);
                    scope.tagsObj = tagsService;
                });

                scope.$watch('autocompleteValue', function () {
                    if (scope.autocompleteValue.length >= charLimit) {
                        scope.recommendedTagsList = [];
                        apiService.call('Tags.getRecommendedTags', {'tag': scope.autocompleteValue}, function (response) {
                            if (response.success) {
                                scope.recommendedTagsList = response.data;
                            }

                            digest(scope.$parent);
                        });
                    } else {
                        scope.recommendedTagsList = [];
                    }
                });

                scope.$watch('recommendedTagsList', function () {
                    scope.toggleElement();
                });

                scope.toggleElement = function (hide) {

                    if (typeof hide !== 'boolean') {
                        hide = false;
                    }

                    if (!hide && scope.recommendedTagsList.length) {
                        $('#autocomplete_' + scope.id).css('left', 'inherit');
                        scope.toShort = false;
                    } else {
                        //@TODO
                        //$('#autocomplete_' + scope.id).css('left', 9999);
                    }

                };

                scope.selected = function (option) {
                    if (option.length >= charLimit) {
                        scope.recommendedTagsList = [];
                        scope.autocompleteValue = '';

                        tagsService.addTag(option);
                        scope.tags = tagsService.getOutgoingTags();
                        scope.toShort = false;
                        digest(scope.$parent);
                    } else {
                        scope.toShort = true;
                    }
                };

                scope.deleteTag = function (index) {
                    tagsService.deleteTag(index);
                    scope.tags = tagsService.getOutgoingTags();
                };
            },
            template: '<input class="autocomplete big-input no-margin" data-ng-class="{toShort:toShort}" placeholder="{{placeholder}}" data-ng-model="autocompleteValue" type="text" click-outside="toggleElement(true)" data-ng-click="toggleElement()" name="tags" data-ng-enter="selected(autocompleteValue)" />'
            + '<p data-ng-show="toShort" class="form-error">' + translate('oneTagTooShort') + '</p>'
            + '<ul id="autocomplete_{{id}}" class="f-dropdown dropdown-menu no-margin" style="width: {{elemWidth}}px; max-width: none;" ng-show="recommendedTagsList.length">'
            + '<li data-ng-cloak data-ng-repeat="option in recommendedTagsList" data-ng-click="selected(option.name)">{{option.name}}</li>'
            + '</ul>'
            + '<p class="tags-holder">' +
            '<span data-ng-cloak data-ng-repeat="tag in tagsObj.tags" class="label-tag">{{tag}} <a href="" class="label-remove" data-ng-click="deleteTag($index)">x</a></span>' +
            '</p>'
        };
    }]);