/**
 * Created by software-consult.ro on 6/23/2015.
 *
 * Used to assemble urls (copies \Library\Mvc\Url that extends \Phalcon\Mvc\Url functionality)
 * Usage:
 *
 * // Default route
 * Url.get({'for': 'account/register'}, {confirmation: 'dssdssdsd'}) => 'baseUri/account/register?confirmation=dssdssdsd'
 *
 * // Custom route (/register/:params)
 * // With parameters in route
 * Url.get({'for': 'account-register', params: 'dssdssdsd'}) => 'baseUri/register/dssdssdsd'
 * // Without parameters in route
 * Url.get({'for': 'account-register'}) => 'baseUri/register'
 *
 */
var Url = {
    routes: [],
    baseUri: '',
    matchedRoute: null,
    /**
     * Initialize the Url object
     *
     * @param routes
     * @param baseUri
     */
    init: function(routes, baseUri) {
        if (typeof routes === 'string') {
            routes = JSON.parse(routes);
        }

        if (typeof routes === 'object') {
            Url.routes = routes;
        }

        if (typeof baseUri === 'string') {

            // add the trailing slash if it dosn't have
            if (baseUri[baseUri.length - 1] !== '/') {
                baseUri += '/';
            }

            Url.baseUri = baseUri;
        }
    },
    /**
     * Remove the trailing slash in a url
     *
     * @param url
     * @returns {*}
     */
    removeTrailingSlash: function(url) {
        if (url[url.length - 1] === '/') {
            url = url.substring(0, url.length - 1);
        }

        return url;
    },
    /**
     * Get the url assembled by given params
     *
     * @param uri
     * @param args
     * @param local
     * @returns {*}
     */
    get: function(uri, args, local) {
        /**
         * Build the url
         *
         * @param url
         * @param args
         * @param local
         *
         * @returns {*}
         *
         * @private
         */
        var _buildUrl = function(url, args, local) {
            // remove the first slash if it has it
            if (url[0] == '/') {
                url = url.substring(1, url.length);
            }

            if (typeof local !== 'boolean' || !local) {
                url = Url.baseUri + url;
            } else {
                url = '/' + url;
            }

            // remove the trailing slash from url
            url = Url.removeTrailingSlash(url);

            if (typeof args === 'object' && args !== null) {
                var paramsArray = [];
                $.each(args, function(key, value) {
                     paramsArray.push(key + '=' + value);
                });

                url = url + '?' + paramsArray.join('&');
            }

            return url;
        };

        /**
         * If we have any params build them
         *
         * @param uri
         * @returns {string}
         * @private
         */
        var _getUriParamsBuilt = function(uri){
            var params = '';
            if (typeof uri.params !== 'undefined') {
                params = uri.params;
                if (typeof uri.params === 'object' && uri.params!==null) {
                    var params = [], key = null;
                    for(key in uri.params) {
                        if (typeof uri.params[key] !== 'undefined' && uri.params[key] !== '' && uri.params[key] !== null) {
                            params.push(uri.params[key]);
                        }
                    }
                    params = params.join('/');
                }

            }

            return params
        };

        Url.matchedRoute = null;
        if (typeof uri !== 'object' || typeof uri.for !== 'string') {
            return uri;
        }

        $.each(Url.routes, function(index, route) {
            if (route.name == uri.for) {
                Url.matchedRoute = route;
                return false;
            }
        });

        // If not a custom route
        if (Url.matchedRoute == null) {
            // Add trailing slash
            if (uri.for[uri.for.length -1] !== '/') {
                uri.for += '/';
            }

            uri.for += _getUriParamsBuilt(uri);

            // Remove the trailing slash
            if (uri.for[uri.for.length -1] == '/') {
                uri.for = uri.for.substring(0, uri.for.length -1);
            }

            return _buildUrl(uri.for, args, local);
        }

        // If a custom route
        var urlParts = Url.matchedRoute.url.split('/');
        var paramsIndexes = [];

        $.each(urlParts, function(index, part){
            if (part[0] == '{' && part[part.length -1] == '}') {
                paramsIndexes.push(index);
                return true;
            }

            // special treatment for params
            if (part[0] == ':' && part != ':params') {
                paramsIndexes.push(index);
                return true;
            }

            if (part[0] == '(' && part[part.length -1] == ')') {
                paramsIndexes.push(index);
                return true;
            }
        });

        if (typeof Url.matchedRoute.route_params === 'string' && Url.matchedRoute.route_params.length) {
            var routeParams = JSON.parse(Url.matchedRoute.route_params);
            $.each(routeParams, function(key, routeParam) {

                // if the route param is only a number then we have to use the params given in the function
                if (/^\d+$/.test(routeParam)) {
                    routeParam = parseInt(routeParam);

                    // if we have the param declared in route and we get the param
                    if (paramsIndexes[routeParam - 1] >= 0 && typeof uri[key] == 'string'){
                        var partIndex = paramsIndexes[routeParam - 1];
                        var part = urlParts[partIndex];
                        if (part[0] !== '(' || part[part.length -1] !== ')') {
                            urlParts[partIndex] = uri[key];
                            return true;
                        }

                        part = new RegExp(part, 'g');
                        if (part.test(uri[key])) {
                            urlParts[partIndex] = uri[key];
                        }
                    }
                }
            });
        }

        // check if we have params route
        if (urlParts[urlParts.length -1] == ':params') {
            urlParts[urlParts.length -1] = _getUriParamsBuilt(uri);
        }

        return _buildUrl(urlParts.join('/'), args, local);
    }
};