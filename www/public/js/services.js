/**
 * Created by software-consult.ro on 7/29/14.
 */

/**
 * Wrapper over api calls
 */
App.service('apiService', ["$http", function ($http) {

    var setExtraParams = function (params) {
        var defaultParams = {
            'method': 'POST',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformResponse: function(response) {
                // First escape the double quote
                response = response.replace(/&#34;/g, '\\"');
                response = response.replace(/&quot;/g, '\\"');

                // Apply html entity decode
                response = JSON.parse($('<div></div>').html(response).text());

                return response;
            },
            withCredentials: true,
            responseType: 'text'
        };

        $.extend(true, defaultParams, params);

        return defaultParams;
    };

    var _nrCalls = 0;
    var _nrCallsDone = 0;
    var makeTheCall = function (config, callback) {
        _nrCalls++;
        $("body").css({cursor:"progress"});
        $http(config).success(function (response) {   // Make the call
            _nrCallsDone++;
            if(_nrCallsDone==_nrCalls)
            {
                $("body").css({cursor:"default"});
            }
            $('#loader').hide();
            if (typeof (callback) === 'function') {     // On success, if callback defined
                callback(response);
            }
        }).error(function (response, status) {
            _nrCallsDone++;
            if(_nrCallsDone==_nrCalls)
            {
                $("body").css({cursor:"default"});
            }
            $('#loader').hide();
            if (typeof (callback) === 'function') {     // On request fail, if callback defined
                response = response || 'HTTP request error - ' + status;
                callback(response);
            }
        });
    };

    var callFunction = function (route, params, callback, extraParams) {
        $('#loader').show();
        //console.log(route,params)
        var routePath = Api.url;

        if (typeof (params) == 'function') {           // Fall back into params order.
            extraParams = callback;
            callback = params;                       // If the method has no params the second param will act like callback
        }

        if (typeof (extraParams) === 'object' && typeof extraParams.apiUrl === 'string') {
            routePath = extraParams.apiUrl;
        }

        if (route.strpos('/') === false) {           // Check if it's a route or not

            if (typeof (params) == 'function' || params === null) {
                params = {};
            }
            routePath += '/call';

            var routeParams = route.split('.');
            if (typeof params !== 'object') {
                params = [params];
            }
            params['application'] = 'Frontend';
            params['proxy'] = routeParams[0];
            params['method'] = routeParams[1];
        } else {
            if (route.strpos('/') === 0) {
                routePath += route;
            } else {
                routePath += '/' + route;
            }
        }

        if (typeof (params) == 'function' || params === null) {
            params = "data=''";
        } else {
            params = "data=" + encodeURIComponent(JSON.stringify(params));
        }

        var tmp = {// Params that cannot be changed
            'url': routePath,
            'data': params
        };

        var config = setExtraParams(extraParams);    // Map extraParams with the default params

        $.extend(true, config, tmp);

        if (typeof LTEIE10 !== 'undefined' && LTEIE10) {
            config.url = "/call";
        }

        makeTheCall(config, callback);

    };

    var callLocalFunction = function (route, params, callback, extraParams) {

        var config = setExtraParams(extraParams);

        if (typeof (params) == 'function') {           // Fall back into params order.
            callback = params;                       // If the method has no params the second param will act like callback
            params = "data=''";
        } else {                                     // If params is a string convert to array
            params = "data=" + JSON.stringify(params);
        }

        $.extend(true, config, extraParams);

        var tmp = {// Params that cannot be changed
            'url': route,
            'data': params
        };

        $.extend(true, config, tmp);

        makeTheCall(config, callback);
    };

    var exportCsv = function (route, params, callback, extraParams) {

        if (typeof (params) == 'function') {           // Fall back into params order.
            extraParams = callback;
            callback = params;                       // If the method has no params the second param will act like callback
        }

        // First make a call to get the data from API
        callFunction(route, params, function (response) {
            if (!response.success) {
                if (typeof callback === 'function') {
                    callback(response);
                }

            } else {
                var paramsToSend = response.data;

                if (typeof params['filename'] !== 'undefined') {
                    paramsToSend.push({'filename': params['filename']});
                } else {
                    paramsToSend.push({'filename': 'adminData'});
                }

                // Save the data to tmp csv file
                callLocalFunction('/csv/saveToFile', paramsToSend, function (res) {
                    // If saved in a file add it to an iframe to be triggered as a download
                    if (res.success) {
                        $('body').append('<iframe id="invisible" style="display:none;"></iframe>');
                        var iframe = document.getElementById('invisible');
                        iframe.src = "/files/export?" + res.data;
                    }
                    if (typeof callback === 'function') {
                        callback(res);
                    }
                });
            }
        }, extraParams);
    };


    var callWithFile = function (route, form, params, callback) {

        form.attr('action', Api.url + "/call/upload");

		if (typeof params === 'function') {
            callback = params;
        }

        // Make the validation
        var files = form.find('[type="file"]');
        if (files.length > 0) {
            var weHaveError = false;
            var error = {};
            $.each(files, function (index, value) {
                var obj = $(value).get(0);
                if (obj.files && obj.files[0].size > Config.img.maxSize) {
                    var theError = {};
                    theError[obj.name] = 'imageTooBig';
                    error = {
                        'success': false,
                        'data': theError
                    };
                    weHaveError = true;
                    return false;
                }
            });

            if (weHaveError) {
                if (typeof callback === 'function') {
                    callback(error);
                }
                return false;
            }
        }

        var routeParams = route.split('.');
        var paramsToSend = {
            'proxy': routeParams[0],
            'method': routeParams[1],
            'application': 'Frontend'
        };

        if (typeof params !== 'function') {
            paramsToSend['form_data'] = JSON.stringify(params);
		}

        $.each(paramsToSend, function (key, val) {
            form.append('<input type="hidden" name="data[' + key + ']" value=\'' + val.replace(/(')/g, "&#39;") + '\' class="tmp_form_input" />');
        });

        form.ajaxSubmit({
            success: function (response) {
                if (typeof callback === 'function') {
                    // Apply html entity decode
                    response = JSON.parse($('<div></div>').html(JSON.stringify(response)).text());
                    callback(response);
                }
                return false;
            },
            error: function (xhr, status, error) {
                var tmp = {'success': false, 'data': 'Ajax call error with message: ' + xhr.statusText};
                if (typeof callback === 'function') {
                    callback(tmp);
                }
                return false;
            },
            complete: function (context, xhr, error) {
                $('.tmp_form_input').remove();
            }
        });
    };

    return {
        call: callFunction,
        callLocal: callLocalFunction,
        exportCsv: exportCsv,
        callWithFile: callWithFile
    };

}]);

/**
 * Errors handler
 */
App.service('errorsHandler', function () {
    /**
     * Handles incoming errors in a given form
     * 
     * @param form
     *                DOM element
     * @param errors
     *                form_element_name: "Error" pairs
     * @param translationPrefix
     */
    this.handle = function (form, errors, translationPrefix) {

        if (typeof (errors) === "undefined" || errors === null
                || errors.length === 0) {
            return;
        }

        if (typeof translationPrefix !== 'string') {
            translationPrefix = '';
        }

        // Remove existing errors
        this.removeErrors(form);
        // Add errors to fields
        $.each(errors, function (key, error) {

            var doTranslate = true;
            if(typeof error == 'object')
            {
                key         = error.key;
                doTranslate = error.translate;
                error       = error.error;
            }
            //console.log(key,doTranslate,error);

            var formElement = form.find("[name='" + key + "']");

            // Check for checkboxes also
            if (formElement.length === 0) {
                formElement = form.find("[name='" + key + "[]']");
            }

            // check if we have an element with the given name
            if (formElement.length) {
                // create errors container
                var errorMsg = $('<p class="form-error"></p>');

                // On the first scrollable ancestors scroll we recalculate the
                // position of the error element
                formElement.addClass('error');
                // Add the error message
                errorMsg.text(doTranslate ? translate(translationPrefix + error) : error);
                formElement.after(errorMsg);
            }
        });
    };

    /**
     * Remove errors from a given form
     * 
     * @param form
     *                DOM element
     */
    this.removeErrors = function (form) {
        // remove all errors
        form.find('.error').removeClass('error');
        form.find('.form-error').remove();
    };

    return this;
});

App.service('popupNotification', ["$modal", function ($modal) {
    this.show = function (title, message, extraParams) {

        // If we do not have a title we do not show anything
        if (typeof title !== 'string' || !title.length) {
            return;
        }
        // If we do not have a message we do not show anything
        if (typeof message !== 'string' || !message.length) {
            return;
        }

        var hideOkBtn = extraParams ? extraParams.hideOkBtn : false;

        var modalHtml = '<div class="tiny">' +
                            '<div class="modal-header gray-text">' +
                                translate(title) +
                                '<a class="close-reveal-modal" ng-click="closeModal()">&#215;</a>' +
                            '</div>' +
                            '<div class="columns block row text-center">' +
                                '<span>' + translate(message) +'</span>' +
                            '</div>' +
                            (!hideOkBtn ? ''+
                            '<div class="modal-footer row">' +
                                '<button class="clear-fix" data-ng-click="closeModal()">' + translate('ok') + '</button>' +
                            '</div>' : '') +
                        '</div>';

        if(typeof extraParams !== "undefined" && typeof extraParams.modalHtml !== "undefined")
        {
            modalHtml = extraParams.modalHtml;
        }

        $modal.open({
            'template': modalHtml,
            'windowClass': 'tiny',
            'backdrop': 'static',
            'controller': 'modalPopupNotificationController'
        });

    };
}]);

App.controller('modalPopupNotificationController', ["$scope", "$modalInstance", function ($scope, $modalInstance) {
    $scope.closeModal = function () {
        $modalInstance.close();
    };
}]);