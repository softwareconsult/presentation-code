<?php

try {

    /**
     * Handle the request
     */
    $application = include __DIR__ . '/../app/Bootstrap.php';


    // Just to start the session
    \Phalcon\Di::getDefault()->get('session');

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo $e->getMessage();
}

